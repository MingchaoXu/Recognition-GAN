import torch
import os
from model_reg import Generator, Dis_crnn
# from utils import *
from torch.serialization import default_restore_location

def load_state(path, model, optimizer=None):
    def map_func(storage, location):
        return storage.cuda()
    if os.path.isfile(path):
        print("=> loading checkpoint '{}'".format(path))
        checkpoint = torch.load(path, map_location=lambda s, l: default_restore_location(s, 'cpu'))
        # print(checkpoint)
        # model.load_state_dict(checkpoint['state_dict'], strict=False)
        pretrain(model, checkpoint)
        ckpt_keys = set(checkpoint.keys())
        # print(ckpt_keys)
        own_keys = set(model.state_dict().keys())
        missing_keys = own_keys - ckpt_keys
        for k in missing_keys:
            print('caution: missing keys from checkpoint {}: {}'.format(path, k))

        if optimizer != None:
            best_prec1 = checkpoint['best_prec1']
            last_iter = checkpoint['step']
            optimizer.load_state_dict(checkpoint['optimizer'])
            print("=> also loaded optimizer from checkpoint '{}' (iter {})"
                  .format(path, last_iter))
            return best_prec1, last_iter
    else:
        print("=> no checkpoint found at '{}'".format(path))

def pretrain(model, state_dict):
    own_state = model.state_dict()
    for name, param in state_dict.items():
        if name in own_state:
            if isinstance(param, torch.nn.Parameter):
                # backwards compatibility for serialized parameters
                param = param.data
            try:
                own_state[name].copy_(param)
            except:
                print('While copying the parameter named {}, '
                      'whose dimensions in the model are {} and '
                      'whose dimensions in the checkpoint are {}.'
                      .format(name, own_state[name].size(), param.size()))
                print("But don't worry about it. Continue pretraining.")

netD = Dis_crnn(imgH=32, nc=3, nclass=37, nh=256, n_rnn=2, leakyRelu=False)
path = '../finetune_model/checkpoint_last.pt'
load_state(path, netD)

