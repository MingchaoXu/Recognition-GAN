import torch
import os
import random
from os import listdir
from os.path import join
import numpy as np

from PIL import Image
from torch.utils.data.dataset import Dataset
from torchvision.transforms import Compose, RandomCrop, ToTensor, ToPILImage, CenterCrop, Resize


def is_image_file(filename):
    return any(filename.endswith(extension) for extension in ['.png', '.jpg', '.jpeg', '.PNG', '.JPG', '.JPEG', '.pgm'])


def calculate_valid_crop_size(crop_size, upscale_factor):
    return crop_size - (crop_size % upscale_factor)


def train_hr_transform(crop_size):
    return Compose([
        ToTensor(),
    ])


def train_lr_transform(crop_size, upscale_factor):
    return Compose([
        ToTensor(),
    ])


def display_transform(w, h):
    new_h = 32
    rate = w / h
    new_w = int(new_h * rate)
    return Compose([
        ToPILImage(),
        # Resize((new_h, new_w)),
        # CenterCrop(400),
        ToTensor()
    ])

class TrainDatasetFromFolderLr(Dataset):
    # for the dataset /data3/cwang/scene_text_recognition/90kDICT32px/
    def __init__(self, dataset_dir, source_file, str2label, crop_size, upscale_factor):
        super(TrainDatasetFromFolderLr, self).__init__()
        self.h_norm = 32
        self.source_file = source_file
        self.upscale_factor = upscale_factor
        self.image_filenames = []
        self.dataset_dir = dataset_dir
        with open(self.source_file, 'r') as fin:
            for i in fin.readlines():
                i = i.rstrip()
                # folder, name = os.path.split(i)
                # name = os.path.splitext(name)[0]
                # gt_name = os.path.join(self.dataset_dir, folder, name+'.txt')
                # with open(gt_name, 'r') as f:
                #     gt = f.readline()
                #     gt = gt.rsplit()[0]
                # # name_list = name.split('_')
                # label_list = [str2label[i.upper()] for i in gt]
                _, name = os.path.split(i)
                name = os.path.splitext(i)[0]
                name_list = name.split('_')
                label_list = [str2label[i.upper()] for i in name_list[1]]
                self.image_filenames.append((i, label_list))
                # print(i, label_list, gt)
        # self.image_filenames = [join(dataset_dir, x) for x in listdir(dataset_dir) if is_image_file(x)]
        crop_size = calculate_valid_crop_size(crop_size, upscale_factor)
        self.hr_transform = train_hr_transform(crop_size)
        self.lr_transform = train_lr_transform(crop_size, upscale_factor)

    def __getitem__(self, index):
        img_path, label_list = self.image_filenames[index]
        img_path = join(self.dataset_dir, img_path)
        # print(label_list)
        label = torch.FloatTensor(label_list)
        ori_img = Image.open(img_path)
        w, h = ori_img.size
        rate = w / h
        # new_w = int(self.h_norm * rate) - (int(self.h_norm * rate) % self.upscale_factor)
        new_w = 100
        new_h = self.h_norm
        try:
            ori_img = ori_img.resize((new_w, new_h), Image.BICUBIC)
        except:
            print(img_path, w, h, new_w, new_h)
        lr_image = ori_img.resize((new_w // self.upscale_factor, new_h // self.upscale_factor), Image.BICUBIC)
        hr_scale = Resize((new_h, new_w), interpolation=Image.BICUBIC)

        hr_image = self.hr_transform(ori_img)
        hr_restore_img = hr_scale(lr_image)
        lr_image = self.lr_transform(lr_image)

        if random.random() < 0.5:
            hr_image = ToTensor()(hr_restore_img)
        return {
            'id': index,
            'lr_image': lr_image,
            'hr_image': hr_image,
            'hr_restore_img': ToTensor()(hr_restore_img),
            'target': label,
        }
        # return lr_image, hr_image

    def __len__(self):
        return len(self.image_filenames)

    def collater(self, samples):
        """Merge a list of samples to form a mini-batch."""
        return collate(
            samples, img_pad_value=0, pad_idx=0,
        )


class TrainDatasetFromFolder2(Dataset):
    # for the dataset /data3/cwang/scene_text_recognition/90kDICT32px/
    def __init__(self, dataset_dir, source_file, str2label, crop_size, upscale_factor, shuffle=False, h_norm=32):
        super(TrainDatasetFromFolder2, self).__init__()
        self.h_norm = crop_size
        self.source_file = source_file
        self.upscale_factor = upscale_factor
        self.image_filenames = []
        self.dataset_dir = dataset_dir
        self.shuffle = shuffle
        with open(self.source_file, 'r') as fin:
            for i in fin.readlines():
                i = i.rstrip()

                # folder, name = os.path.split(i)
                # name = os.path.splitext(name)[0]
                # gt_name = os.path.join(self.dataset_dir, folder, name+'.txt')
                # with open(gt_name, 'r') as f:
                #     gt = f.readline()
                #     gt = gt.rsplit()[0]
                # # name_list = name.split('_')
                # label_list = [str2label[i.upper()] for i in gt]
                # _, name = os.path.split(i)
                # name = os.path.splitext(i)[0]
                # name_list = name.split('_')
                # label_list = [str2label[i.upper()] for i in name_list[1]]

                # folder, name = os.path.split(i)
                # name = os.path.splitext(name)[0]
                # # print(name)
                # gt_name = os.path.join(self.dataset_dir, folder, name+'_gt.txt')
                # with open(gt_name, 'r') as f:
                #     gt = f.readline()
                #     gt = gt.rsplit()[0]
                #     gt = gt.split(' ')
                #     gt = [int(i) for i in gt]
                # # name_list = name.split('_')

                _, name = os.path.split(i)
                name = os.path.splitext(i)[0]
                name_list = name.split('_')
                label_list = [str2label[i.upper()] for i in name_list[1]]
                # import pdb; pdb.set_trace()
                self.image_filenames.append((i, label_list))
                
                # label_list = gt

                # self.image_filenames.append((i, label_list))

                # print(i, label_list, gt)
        # self.image_filenames = [join(dataset_dir, x) for x in listdir(dataset_dir) if is_image_file(x)]
        crop_size = calculate_valid_crop_size(crop_size, upscale_factor)
        self.hr_transform = train_hr_transform(crop_size)
        self.lr_transform = train_lr_transform(crop_size, upscale_factor)

    def __getitem__(self, index):
        img_path, label_list = self.image_filenames[index]
        img_path = join(self.dataset_dir, img_path)
        # print(label_list)
        label = torch.FloatTensor(label_list)
        ori_img = Image.open(img_path).convert('L')
        ori_image = ToTensor()(ori_img)
        ori_image = torch.cat((ori_image, ori_image, ori_image), 0)
        w, h = ori_img.size
        rate = w / h
        # new_w = int(self.h_norm * rate) - (int(self.h_norm * rate) % self.upscale_factor)
        new_w = 100
        new_h = self.h_norm
        try:
            ori_img = ori_img.resize((new_w, new_h), Image.BICUBIC)
        except:
            print(img_path, w, h, new_w, new_h)
        lr_image = ori_img.resize((new_w // self.upscale_factor, new_h // self.upscale_factor), Image.BICUBIC)
        hr_scale = Resize((new_h, new_w), interpolation=Image.BICUBIC)

        hr_image = self.hr_transform(ori_img)
        hr_image = torch.cat((hr_image, hr_image, hr_image), 0)
        hr_restore_img = ToTensor()(hr_scale(lr_image))
        hr_restore_img = torch.cat((hr_restore_img, hr_restore_img, hr_restore_img), 0)
        lr_image = self.lr_transform(lr_image)
        lr_image = torch.cat((lr_image, lr_image, lr_image), 0)
        # print(hr_image.size(), lr_image.size())
        return {
            'id': index,
            'ori_image': ori_image, 
            'lr_image': lr_image,
            'hr_image': hr_image,
            'hr_restore_img': hr_restore_img,
            'target': label,
        }
        # return lr_image, hr_image

    def __len__(self):
        return len(self.image_filenames)

    def collater(self, samples):
        """Merge a list of samples to form a mini-batch."""
        return collate(
            samples, img_pad_value=0, pad_idx=0,
        )
    def ordered_indices(self):
        """Ordered indices for batching."""
        if self.shuffle:
            data_len = len(self)
            batch_num = data_len // self.batch_size
            bt_list = random.sample(list(range(data_len)), batch_num)
            indices_list = []
            for i in bt_list:
                if i > data_len - self.batch_size:
                    tmp_list = list(range(data_len - self.batch_size, data_len))
                else:
                    tmp_list = list(range(i, i + self.batch_size))
                indices_list += tmp_list
            indices = np.array(indices_list)
            # indices = np.random.permutation(len(self))

        else:
            indices = np.arange(len(self))
        # if self.tgt_sizes is not None:
        #     indices = indices[np.argsort(self.tgt_sizes[indices], kind='mergesort')]
        # 返回的是一个dnarry，里面的元素是句子的id，已经做过乱序，并且按照句子的长度排序
        # return indices[np.argsort(self.src_sizes[indices], kind='mergesort')]
        return indices

    def num_tokens(self, index):
        """Return an example's length (number of tokens), used for batching."""
        return self.src_sizes[index]

    def src_sizes(self):
        # 返回一个ndarray， 每个元素的大小是对应句子的长度
        src_sizes = []
        for i in range(0, len(self)):
            src_sizes.append(len(self[i]['target']))
        return np.array(src_sizes)



class ValDatasetFromFolder2(Dataset):
    # for the dataset /data3/cwang/scene_text_recognition/90kDICT32px/
    def __init__(self, dataset_dir, source_file, str2label, upscale_factor, h_norm=32):
        super(ValDatasetFromFolder2, self).__init__()
        self.h_norm = h_norm
        self.upscale_factor = upscale_factor
        self.source_file = source_file
        self.image_filenames = []
        self.dataset_dir = dataset_dir
        with open(self.source_file, 'r') as fin:
            for i in fin.readlines():
                i = i.rstrip()
                # folder, name = os.path.split(i)
                # name = os.path.splitext(name)[0]
                # gt_name = os.path.join(self.dataset_dir, folder, name+'.txt')
                # with open(gt_name, 'r') as f:
                #     gt = f.readline()
                #     gt = gt.rsplit()[0]
                # # name_list = name.split('_')
                # label_list = [str2label[i.upper()] for i in gt]
                # _, name = os.path.split(i)
                # name = os.path.splitext(i)[0]
                # name_list = name.split('_')
                # label_list = [str2label[i.upper()] for i in name_list[1]]

                # folder, name = os.path.split(i)
                # # black list
                # # name = os.path.splitext(name)[0][:-6]
                # name = os.path.splitext(name)[0]
                # gt_name = os.path.join(self.dataset_dir, folder, name+'_gt.txt')
                # with open(gt_name, 'r') as f:
                #     gt = f.readline()
                #     gt = gt.rsplit()[0]
                #     gt = gt.split(' ')
                #     gt = [int(i) for i in gt]
                # # name_list = name.split('_')
                
                # label_list = gt

                # self.image_filenames.append((i, label_list))

                _, name = os.path.split(i)
                name = os.path.splitext(i)[0]
                name_list = name.split('_')
                label_list = [str2label[i.upper()] for i in name_list[1]]
                # import pdb; pdb.set_trace()
                self.image_filenames.append((i, label_list))
        
        # self.image_filenames = [join(dataset_dir, x) for x in listdir(dataset_dir) if is_image_file(x)]

    def __getitem__(self, index):
        img_path, label_list = self.image_filenames[index]
        # print(self.image_filenames[0])
        img_path = join(self.dataset_dir, img_path)
        label = torch.FloatTensor(label_list)
        # ori_image = Image.open(img_path)
        hr_image = Image.open(img_path).convert('L')
        
        ori_image = ToTensor()(hr_image)
        ori_image = torch.cat((ori_image, ori_image, ori_image), 0)
        w, h = hr_image.size
        rate = w / h
        # new_w = int(self.h_norm * rate) - (int(self.h_norm * rate) % self.upscale_factor)
        new_w = 100
        new_h = self.h_norm
        # 强行resize成 32， 100
        hr_image = hr_image.resize((new_w, new_h), Image.BICUBIC)
        
        crop_size = calculate_valid_crop_size(min(w, h), self.upscale_factor)

        lr_scale = Resize((new_h // self.upscale_factor, new_w // self.upscale_factor), interpolation=Image.BICUBIC)
        hr_scale = Resize((new_h, new_w), interpolation=Image.BICUBIC)
        lr_image = lr_scale(hr_image)
        hr_restore_img = ToTensor()(hr_scale(lr_image))
        lr_image = ToTensor()(lr_image)
        hr_image = ToTensor()(CenterCrop((new_h, new_w))(hr_image))
        hr_image = torch.cat((hr_image, hr_image, hr_image), 0)
        lr_image = torch.cat((lr_image, lr_image, lr_image), 0)
        hr_restore_img = torch.cat((hr_restore_img, hr_restore_img, hr_restore_img), 0)
        return {
            'id': index,
            'ori_image': ori_image, 
            'lr_image': lr_image,
            'hr_restore_img': hr_restore_img,
            'hr_image': hr_image,
            'target': label,
        }
        # return ToTensor()(lr_image), ToTensor()(hr_restore_img), ToTensor()(hr_image)

    def __len__(self):
        return len(self.image_filenames)

    def collater(self, samples):
        """Merge a list of samples to form a mini-batch."""
        return collate_val(
            samples, img_pad_value=0, pad_idx=0,
        )
    def src_sizes(self):
        # 返回一个ndarray， 每个元素的大小是对应句子的长度
        src_sizes = []
        for i in range(0, len(self)):
            src_sizes.append(len(self[i]['target']))
        return np.array(src_sizes)

class ValDatasetFromFolder_generate(Dataset):
    # for the dataset /data3/cwang/scene_text_recognition/90kDICT32px/
    def __init__(self, dataset_dir, source_file, str2label, upscale_factor, h_norm=32):
        super(ValDatasetFromFolder_generate, self).__init__()
        self.h_norm = h_norm
        self.upscale_factor = upscale_factor
        self.source_file = source_file
        self.image_filenames = []
        self.dataset_dir = dataset_dir
        with open(self.source_file, 'r') as fin:
            for i in fin.readlines():
                i = i.rstrip()
                # folder, name = os.path.split(i)
                # name = os.path.splitext(name)[0]
                # gt_name = os.path.join(self.dataset_dir, folder, name+'.txt')
                # with open(gt_name, 'r') as f:
                #     gt = f.readline()
                #     gt = gt.rsplit()[0]
                # # name_list = name.split('_')
                # label_list = [str2label[i.upper()] for i in gt]
                _, name = os.path.split(i)
                name = os.path.splitext(i)[0]
                name_list = name.split('_')
                label_list = [str2label[i.upper()] for i in name_list[1]]
                # import pdb; pdb.set_trace()
                
                self.image_filenames.append((i, label_list))
        
        # self.image_filenames = [join(dataset_dir, x) for x in listdir(dataset_dir) if is_image_file(x)]

    def __getitem__(self, index):
        img_path, label_list = self.image_filenames[index]
        # print(self.image_filenames[0])
        img_path = join(self.dataset_dir, img_path)
        label = torch.FloatTensor(label_list)
        # print(img_path)
        # ori_image = Image.open(img_path)
        hr_image = Image.open(img_path).convert('L')
        
        
        ori_image = ToTensor()(hr_image)
        # # print(ori_image.size())
        ori_image = torch.cat((ori_image, ori_image, ori_image), 0)
        # print(ori_image.size())
        w, h = hr_image.size
        rate = w / h
        # new_w = int(self.h_norm * rate) - (int(self.h_norm * rate) % self.upscale_factor)
        new_w = 100
        new_h = self.h_norm
        # 强行resize成 32， 100
        hr_image = hr_image.resize((new_w, new_h), Image.BICUBIC)
        
        crop_size = calculate_valid_crop_size(min(w, h), self.upscale_factor)

        lr_scale = Resize((new_h // self.upscale_factor, new_w // self.upscale_factor), interpolation=Image.BICUBIC)
        hr_scale = Resize((new_h, new_w), interpolation=Image.BICUBIC)
        lr_image = lr_scale(hr_image)
        hr_restore_img = ToTensor()(hr_scale(lr_image))
        lr_image = ToTensor()(lr_image)
        hr_image = ToTensor()(CenterCrop((new_h, new_w))(hr_image))
        hr_image = torch.cat((hr_image, hr_image, hr_image), 0)
        lr_image = torch.cat((lr_image, lr_image, lr_image), 0)
        hr_restore_img = torch.cat((hr_restore_img, hr_restore_img, hr_restore_img), 0)
        return {
            'id': index,
            'ori_image': ori_image, 
            'lr_image': lr_image,
            'hr_restore_img': hr_restore_img,
            'hr_image': hr_image,
            'target': label,
        }
        # return ToTensor()(lr_image), ToTensor()(hr_restore_img), ToTensor()(hr_image)

    def __len__(self):
        return len(self.image_filenames)

    def collater(self, samples):
        """Merge a list of samples to form a mini-batch."""
        return collate_val(
            samples, img_pad_value=0, pad_idx=0,
        )

class TrainDatasetFromFolder(Dataset):
    def __init__(self, dataset_dir, source_file, crop_size, upscale_factor):
        super(TrainDatasetFromFolder, self).__init__()
        self.h_norm = 64
        self.source_file = source_file
        self.upscale_factor = upscale_factor
        self.image_filenames = []
        with open(self.source_file, 'r') as fin:
            for i in fin.readlines():
                i = i.rstrip()
                name = os.path.splitext(i)[0]
                name_list = name.split('-')
                name_list[1] = 'annot'
                annot = '-'.join(name_list) + '.idx'
                annot = join(os.path.split(dataset_dir)[0], 'ANNOTATION', annot)
                with open(annot, 'r') as f_label:
                    line = f_label.readline().rstrip()
                    label_list = [int(i) for i in line.split(' ')]
                self.image_filenames.append((join(dataset_dir, i), label_list))
        # self.image_filenames = [join(dataset_dir, x) for x in listdir(dataset_dir) if is_image_file(x)]
        crop_size = calculate_valid_crop_size(crop_size, upscale_factor)
        self.hr_transform = train_hr_transform(crop_size)
        self.lr_transform = train_lr_transform(crop_size, upscale_factor)

    def __getitem__(self, index):
        img_path, label_list = self.image_filenames[index]
        label = torch.FloatTensor(label_list)
        ori_img = Image.open(img_path)
        w, h = ori_img.size
        rate = w / h
        new_w = int(self.h_norm * rate) - (int(self.h_norm * rate) % self.upscale_factor)
        new_h = self.h_norm
        ori_img = ori_img.resize((new_w, new_h), Image.BICUBIC)
        lr_image = ori_img.resize((new_w // self.upscale_factor, new_h // self.upscale_factor), Image.BICUBIC)

        hr_image = self.hr_transform(ori_img)

        lr_image = self.lr_transform(lr_image)
        return {
            'id': index,
            'lr_image': lr_image,
            'hr_image': hr_image,
            'target': label,
        }
        # return lr_image, hr_image

    def __len__(self):
        return len(self.image_filenames)

    def collater(self, samples):
        """Merge a list of samples to form a mini-batch."""
        return collate(
            samples, img_pad_value=0, pad_idx=0,
        )


class ValDatasetFromFolder(Dataset):
    def __init__(self, dataset_dir, source_file, upscale_factor):
        super(ValDatasetFromFolder, self).__init__()
        self.h_norm = 64
        self.upscale_factor = upscale_factor
        self.source_file = source_file
        self.image_filenames = []
        with open(self.source_file, 'r') as fin:
            for i in fin.readlines():
                i = i.rstrip()
                name = os.path.splitext(i)[0]
                name_list = name.split('-')
                name_list[1] = 'annot'
                annot = '-'.join(name_list) + '.idx'
                annot = join(os.path.split(dataset_dir)[0], 'ANNOTATION', annot)
                with open(annot, 'r') as f_label:
                    line = f_label.readline().rstrip()
                    label_list = [int(i) for i in line.split(' ')]
                self.image_filenames.append((join(dataset_dir, i), label_list))
        # self.image_filenames = [join(dataset_dir, x) for x in listdir(dataset_dir) if is_image_file(x)]

    def __getitem__(self, index):
        img_path, label_list = self.image_filenames[index]
        label = torch.FloatTensor(label_list)
        hr_image = Image.open(img_path)
        w, h = hr_image.size
        rate = w / h
        new_w = int(self.h_norm * rate) - (int(self.h_norm * rate) % self.upscale_factor)
        new_h = self.h_norm

        crop_size = calculate_valid_crop_size(min(w, h), self.upscale_factor)

        lr_scale = Resize((new_h // self.upscale_factor, new_w // self.upscale_factor), interpolation=Image.BICUBIC)
        hr_scale = Resize((new_h, new_w), interpolation=Image.BICUBIC)
        hr_image = CenterCrop((new_h, new_w))(hr_image)
        lr_image = lr_scale(hr_image)
        hr_restore_img = hr_scale(lr_image)
        return {
            'id': index,
            'lr_image': ToTensor()(lr_image),
            'hr_restore_img': ToTensor()(hr_restore_img),
            'hr_image': ToTensor()(hr_image),
            'target': label,
        }
        # return ToTensor()(lr_image), ToTensor()(hr_restore_img), ToTensor()(hr_image)

    def __len__(self):
        return len(self.image_filenames)

    def collater(self, samples):
        """Merge a list of samples to form a mini-batch."""
        return collate_val(
            samples, img_pad_value=0, pad_idx=0,
        )


class TestDatasetFromFolder(Dataset):
    def __init__(self, dataset_dir, upscale_factor):
        super(TestDatasetFromFolder, self).__init__()
        self.lr_path = dataset_dir + '/SRF_' + str(upscale_factor) + '/data/'
        self.hr_path = dataset_dir + '/SRF_' + str(upscale_factor) + '/target/'
        self.upscale_factor = upscale_factor
        self.lr_filenames = [join(self.lr_path, x) for x in listdir(self.lr_path) if is_image_file(x)]
        self.hr_filenames = [join(self.hr_path, x) for x in listdir(self.hr_path) if is_image_file(x)]

    def __getitem__(self, index):
        image_name = self.lr_filenames[index].split('/')[-1]
        lr_image = Image.open(self.lr_filenames[index])
        w, h = lr_image.size
        hr_image = Image.open(self.hr_filenames[index])
        hr_scale = Resize((self.upscale_factor * h, self.upscale_factor * w), interpolation=Image.BICUBIC)
        hr_restore_img = hr_scale(lr_image)
        return image_name, ToTensor()(lr_image), ToTensor()(hr_restore_img), ToTensor()(hr_image)

    def __len__(self):
        return len(self.lr_filenames)

    def collater(self, samples):
        """Merge a list of samples to form a mini-batch."""
        return collate(
            samples, img_pad_value=0, pad_idx=None,
        )


def collate(samples, img_pad_value, pad_idx, left_pad_source=True, left_pad_target=False, ctc_flag=False):
    if len(samples) == 0:
        return {}

    def merge(key, left_pad, move_eos_to_beginning=False, ctc_flag=False, sort_order=None):
        # source 是图像，要不同的处理
        if key == 'lr_image' or key == 'hr_image'  or key == 'hr_restore_img':
            # img_list = []
            # img_list.append(collate_tokens([s[key] for s in samples], img_pad_value))
            imgs = collate_tokens([s[key] for s in samples], img_pad_value)
            # hr_image = collate_tokens([s[1] for s in samples], img_pad_value)
            # [s[key] for s in samples],
            return imgs
            # return (lr_image, hr_image)
        elif key == 'ori_image': 
            img_list = []
            # print(samples)
            # for s in samples:
                # print('ori_image', s[key].size()) 
            img_list = [s[key] for s in samples]
            # print('img_list:', len(img_list))
            return img_list
        else:
            # 文本数据
            return crnn_collate_tokens(
                [s[key] for s in samples],
                pad_idx, left_pad, move_eos_to_beginning, ctc_flag=ctc_flag, sort_order=sort_order
            )

    id = torch.LongTensor([s['id'] for s in samples])
    lr_image = merge('lr_image', left_pad=left_pad_source)
    hr_image = merge('hr_image', left_pad=left_pad_source)
    hr_restore_img = merge('hr_restore_img', left_pad=left_pad_source)
    ori_image_tmp = merge('ori_image', left_pad=left_pad_source)
    # sort by descending source length
    # src_lengths 存放的是每个sample的图片的长度， C,H,W
    src_lengths = torch.LongTensor([s['hr_image'].size(2) for s in samples])
    src_lengths, sort_order = src_lengths.sort(descending=False)
    id = id.index_select(0, sort_order)
    lr_image = lr_image.index_select(0, sort_order)
    hr_image = hr_image.index_select(0, sort_order)
    hr_restore_img = hr_restore_img.index_select(0, sort_order)
    ori_image = []
    # print(len(ori_image_tmp), sort_order.size(), sort_order)
    for i in range(sort_order.size()[0]):
        # print(ori_image_tmp[sort_order[i]].size(), ori_image_tmp[0].size(), sort_order[i])
        ori_image.append(ori_image_tmp[sort_order[i]])

    if samples[0].get('target', None) is not None:
        # if not ctc_flag:
        target = merge('target', left_pad=left_pad_target)
        # we create a shifted version of targets for feeding the
        # previous output token(s) into the next decoder step
        prev_output_tokens = merge(
            'target',
            left_pad=left_pad_target,
            move_eos_to_beginning=True,
        )
        # prev_output_tokens = prev_output_tokens.index_select(0, sort_order)
        # target = target.index_select(0, sort_order).long()
        # ntokens = sum(len(s['target']) for s in samples)
        # else:
        target_before_ctc = merge('target', left_pad=left_pad_target)
        target_before_ctc = target_before_ctc.index_select(0, sort_order).long()
        target_ctc = merge('target', left_pad=left_pad_target, ctc_flag=True, sort_order=sort_order)
        # ntokens_ctc = torch.IntTensor([len(s['target']) for s in samples]).numpy()
        ntokens_ctc = torch.IntTensor([len(s['target']) for s in samples]).index_select(0, sort_order).numpy()
    else:
        ntokens = sum(s['source'].size(2) for s in samples)

    return {
        'id': id,
        'ori_image': ori_image,
        'lr_image': lr_image,
        'hr_image': hr_image,
        'hr_restore_img': hr_restore_img,
        'target': target_ctc,
        'ntokens': ntokens_ctc,
        'target_before': target_before_ctc
    }
    # return src_tokens


def collate_val(samples, img_pad_value, pad_idx, left_pad_source=True, left_pad_target=False, ctc_flag=False):
    if len(samples) == 0:
        return {}

    def merge(key, left_pad, move_eos_to_beginning=False, ctc_flag=False, sort_order=None):
        # source 是图像，要不同的处理
        if key == 'lr_image' or key == 'hr_image' or key == 'hr_restore_img':
            # img_list = []
            # img_list.append(collate_tokens([s[key] for s in samples], img_pad_value))
            imgs = collate_tokens([s[key] for s in samples], img_pad_value)
            # hr_image = collate_tokens([s[1] for s in samples], img_pad_value)
            # [s[key] for s in samples],
            return imgs
            # return (lr_image, hr_image)
        elif key == 'ori_image': 
            img_list = []
            # print(samples)
            # for s in samples:
                # print('ori_image', s[key].size()) 
            img_list = [s[key] for s in samples]
            # print('img_list:', len(img_list))
            return img_list
        else:
            # 文本数据
            return crnn_collate_tokens(
                [s[key] for s in samples],
                pad_idx, left_pad, move_eos_to_beginning, ctc_flag=ctc_flag, sort_order=sort_order
            )

    id = torch.LongTensor([s['id'] for s in samples])
    lr_image = merge('lr_image', left_pad=left_pad_source)
    hr_image = merge('hr_image', left_pad=left_pad_source)
    hr_restore_img = merge('hr_restore_img', left_pad=left_pad_source)
    ori_image_tmp = merge('ori_image', left_pad=left_pad_source)
    # print('ori_image_tmp', len(ori_image_tmp))

    # sort by descending source length
    # src_lengths 存放的是每个sample的图片的长度， C,H,W
    src_lengths = torch.LongTensor([s['hr_image'].size(2) for s in samples])
    src_lengths, sort_order = src_lengths.sort(descending=False)
    id = id.index_select(0, sort_order)
    lr_image = lr_image.index_select(0, sort_order)
    hr_image = hr_image.index_select(0, sort_order)
    hr_restore_img = hr_restore_img.index_select(0, sort_order)
    ori_image = []
    # print(len(ori_image_tmp), sort_order.size(), sort_order)
    for i in range(sort_order.size()[0]):
        # print(ori_image_tmp[sort_order[i]].size(), ori_image_tmp[0].size(), sort_order[i])
        ori_image.append(ori_image_tmp[sort_order[i]])
    # print(ori_image[0].size())
    if samples[0].get('target', None) is not None:
        # if not ctc_flag:
        target = merge('target', left_pad=left_pad_target)
        # we create a shifted version of targets for feeding the
        # previous output token(s) into the next decoder step
        prev_output_tokens = merge(
            'target',
            left_pad=left_pad_target,
            move_eos_to_beginning=True,
        )
        # prev_output_tokens = prev_output_tokens.index_select(0, sort_order)
        # target = target.index_select(0, sort_order).long()
        # ntokens = sum(len(s['target']) for s in samples)
        # else:
        target_before_ctc = merge('target', left_pad=left_pad_target)
        target_before_ctc = target_before_ctc.index_select(0, sort_order).long()
        target_ctc = merge('target', left_pad=left_pad_target, ctc_flag=True, sort_order=sort_order)
        # ntokens_ctc = torch.IntTensor([len(s['target']) for s in samples]).numpy()
        ntokens_ctc = torch.IntTensor([len(s['target']) for s in samples]).index_select(0, sort_order).numpy()
    else:
        ntokens = sum(s['source'].size(2) for s in samples)
    # print(ori_image)
    return {
        'id': id,
        'ori_image': ori_image,
        'lr_image': lr_image,
        'hr_image': hr_image,
        'hr_restore_img': hr_restore_img,
        'target': target_ctc,
        'ntokens': ntokens_ctc,
        'target_before': target_before_ctc
    }


def crnn_collate_tokens(values, pad_idx, left_pad, move_eos_to_beginning=False, ctc_flag=False, sort_order=None):
    """Convert a list of 1d tensors into a padded 2d tensor."""

    def copy_tensor(src, dst):
        assert dst.numel() == src.numel()
        dst.copy_(src)

    if not ctc_flag:
        size = max(v.size(0) for v in values)
        res = values[0].new(len(values), size).fill_(pad_idx)
        for i, v in enumerate(values):
            copy_tensor(v, res[i][size - len(v):] if left_pad else res[i][:len(v)])
    else:
        size = sum(v.size(0) for v in values)
        res = values[0].new(size).fill_(pad_idx)
        start = 0
        if sort_order is not None:
            for i in sort_order:
                v = values[i]
                copy_tensor(v, res[start:start + len(v)])
                start += len(v)
        else:
            for i, v in enumerate(values):
                copy_tensor(v, res[start:start + len(v)])
                start += len(v)

    return res


def collate_tokens(values, pad_value=0, resize_height=True, height_norm=128):
    """Convert a list of 3d tensors into a padded 4d tensor. 对图像从中间padding"""

    # C,H,W
    h_size = max(v.size(1) for v in values)
    w_size = max(v.size(2) for v in values)
    # 先构造一个新的tensor作为网络输入的tensor， 是一个二维的tensor，（31，128），31个句子，每句128个token
    res = values[0].new(len(values), values[0].size(0), h_size, w_size).fill_(pad_value)

    def copy_tensor(src, dst):
        # print(dst.size(), src.size())
        assert dst.numel() == src.numel()
        dst.copy_(src)

    for i, v in enumerate(values):
        # print(h_size, w_size, v.size(1), v.size(2), v.size())
        h_tmp_half = (h_size - v.size(1)) // 2
        w_tmp_half = (w_size - v.size(2)) // 2
        # print(h_tmp_half, v.size(1))
        copy_tensor(v, res[i][:, h_tmp_half:h_tmp_half + v.size(1), w_tmp_half:w_tmp_half + v.size(2)])
    return res


def collate_tokens2list(values, pad_value=0, resize_height=True, height_norm=128):
    """Convert a list of 3d tensors into a padded 4d tensor. 对图像从中间padding"""

    # C,H,W
    h_size = max(v.size(1) for v in values)
    w_size = max(v.size(2) for v in values)

    # 先构造一个新的tensor作为网络输入的tensor， 是一个二维的tensor，（31，128），31个句子，每句128个token

    def copy_tensor(src, dst):
        # print(dst.size(), src.size())
        assert dst.numel() == src.numel()
        dst.copy_(src)

    out_list = []
    for i, v in enumerate(values):
        res = values[0].new(values[0].size(0), h_size, w_size).fill_(pad_value)

        # print(h_size, w_size, v.size(1), v.size(2), v.size())
        h_tmp_half = (h_size - v.size(1)) // 2
        w_tmp_half = (w_size - v.size(2)) // 2
        # print(h_tmp_half, v.size(1))
        copy_tensor(v, res[:, h_tmp_half:h_tmp_half + v.size(1), w_tmp_half:w_tmp_half + v.size(2)])
        out_list.append(res)
    return out_list


if __name__ == "__main__":
    dataset_dir = '/data3/cwang/scene_text_recognition/90kDICT32px'
    source_file = '/data3/cwang/scene_text_recognition/90kDICT32px/annotation_train.txt'
    labelstr = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
    str2label = {}
    label2str = {}
    for idx, char in enumerate(labelstr, start=1):
        str2label[char] = idx

    for key in str2label.keys():
        label2str[str(str2label[key])] = key
    crop_size = 64
    upscale_factor = 4
    dataset = TrainDatasetFromFolder2(dataset_dir=dataset_dir, source_file=source_file, str2label=str2label,
                                      crop_size=crop_size, upscale_factor=upscale_factor)
    out = dataset[0]