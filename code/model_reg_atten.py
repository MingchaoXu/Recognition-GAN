import math
import torch

import torch.nn.functional as F
from torch import nn
from modules import BeamableMM, SELayer

class ModelAll(nn.Module):
    def __init__(self, scale_factor, imgH, nc, nclass, nh=256, n_rnn=2, leakyRelu=False):
        super(ModelAll, self).__init__()
        # self.netG = Generator(scale_factor)
        # self.netD = Dis_crnn(imgH=imgH, nc=nc, nclass=nclass, nh=nh, n_rnn=n_rnn, leakyRelu=leakyRelu)

        # generator
        upsample_block_num = int(math.log(scale_factor, 2))
        self.block1 = nn.Sequential(
            nn.Conv2d(3, 64, kernel_size=9, padding=4),
            nn.PReLU()
        )
        self.block2 = ResidualBlock(64)
        self.se2 = SELayer(64*24, 16)
        self.block3 = ResidualBlock(64)
        self.se3 = SELayer(64 * 24, 16)
        self.block4 = ResidualBlock(64)
        self.se4 = SELayer(64 * 24, 16)
        self.block5 = ResidualBlock(64)
        self.se5 = SELayer(64 * 24, 16)
        self.block6 = ResidualBlock(64)
        self.se6 = SELayer(64 * 24, 16)
        self.block7 = nn.Sequential(
            nn.Conv2d(64, 64, kernel_size=3, padding=1),
            nn.PReLU()
        )
        block8 = [UpsampleBLock(64, 2) for _ in range(upsample_block_num)]
        block8.append(nn.Conv2d(64, 3, kernel_size=9, padding=4))
        self.block8 = nn.Sequential(*block8)

        # discriminator
        ks = [3, 3, 3, 3, 3, 3, (2, 3)]
        ps = [1, 1, 1, 1, 1, 1, (0, 1)]
        ss = [1, 1, 1, 1, 1, 1, (2, 1)]
        nm = [64, 128, 256, 256, 512, 512, 512]

        cnn = nn.Sequential()

        def convRelu(i, batchNormalization=False):
            nIn = nc if i == 0 else nm[i - 1]
            nOut = nm[i]
            cnn.add_module('conv{0}'.format(i),
                           nn.Conv2d(nIn, nOut, ks[i], ss[i], ps[i]))
            if batchNormalization:
                cnn.add_module('batchnorm{0}'.format(i), nn.BatchNorm2d(nOut))
            if leakyRelu:
                cnn.add_module('relu{0}'.format(i),
                               nn.LeakyReLU(0.2, inplace=True))
            else:
                cnn.add_module('relu{0}'.format(i), nn.ReLU(True))

        convRelu(0)
        cnn.add_module('pooling{0}'.format(0), nn.MaxPool2d(2, 2))  # 64x16x64
        convRelu(1)
        cnn.add_module('pooling{0}'.format(1), nn.MaxPool2d(2, 2))  # 128x8x32
        convRelu(2, True)
        convRelu(3)
        cnn.add_module('pooling{0}'.format(2),
                       nn.MaxPool2d((2, 3), (2, 1), (0, 1)))  # 256x4x16
        convRelu(4, True)
        convRelu(5)
        cnn.add_module('pooling{0}'.format(3),
                       nn.MaxPool2d((2, 3), (2, 1), (0, 1)))  # 512x2x16
        convRelu(6, True)  # 512x1x16


        self.cnn = cnn
        self.rnn = nn.Sequential(
            BidirectionalLSTM(512, nh, nh),
            BidirectionalLSTM(nh, nh, nclass))

        self.dis = nn.Sequential(nn.AdaptiveAvgPool2d(1),
                                 nn.Conv2d(512, 1024, kernel_size=1),
                                 nn.LeakyReLU(0.2),
                                 nn.Conv2d(1024, 1, kernel_size=1))

    def forward(self, x):
        batch_size = x.size(0)
        conv = self.cnn(x)
        b, c, h, w = conv.size()
        q = conv
        # print(q.size())

        # print(conv.size())
        # assert h == 1, "the height of conv must be 1"
        dis_prob = self.dis(conv)
        dis_prob = F.sigmoid(dis_prob.view(batch_size))
        # print(dis_prob.size())

        conv = conv.squeeze(2)
        conv = conv.permute(2, 0, 1)  # [w, b, c]
        # rnn features
        output = self.rnn(conv)

        block1 = self.block1(x)
        block2 = self.block2(block1)
        block2 = self.se2(block2, q)
        # print(block2.size())
        block3 = self.block3(block2)
        block3 = self.se3(block3, q)
        # print(block2.size())
        block4 = self.block4(block3)
        block4 = self.se4(block4, q)
        # print(block2.size())
        block5 = self.block5(block4)
        block5 = self.se5(block5, q)
        # print(block2.size())
        block6 = self.block6(block5)
        block6 = self.se6(block6, q)
        # print(block2.size())
        block7 = self.block7(block6)
        # block7 = self.se7(block7, q)
        # print(block2.size())
        block8 = self.block8(block1 + block7)
        # return block8
        return (F.tanh(block8) + 1) / 2


class Generator(nn.Module):
    def __init__(self, scale_factor):
        upsample_block_num = int(math.log(scale_factor, 2))

        super(Generator, self).__init__()
        self.block1 = nn.Sequential(
            nn.Conv2d(3, 64, kernel_size=9, padding=4),
            nn.PReLU()
        )
        self.block2 = ResidualBlock(64)
        self.se2 = SELayer(64 * 24, 16)
        self.block3 = ResidualBlock(64)
        self.se3 = SELayer(64 * 24, 16)
        self.block4 = ResidualBlock(64)
        self.se4 = SELayer(64 * 24, 16)
        self.block5 = ResidualBlock(64)
        self.se5 = SELayer(64 * 24, 16)
        self.block6 = ResidualBlock(64)
        self.se6 = SELayer(64 * 24, 16)
        self.block7 = nn.Sequential(
            nn.Conv2d(64, 64, kernel_size=3, padding=1),
            nn.PReLU()
        )
        block8 = [UpsampleBLock(64, 2) for _ in range(upsample_block_num)]
        block8.append(nn.Conv2d(64, 3, kernel_size=9, padding=4))
        self.block8 = nn.Sequential(*block8)

    def forward(self, x, q=None):
        if q is None:
            block1 = self.block1(x)
            block2 = self.block2(block1)
            # print(block2.size())
            # print(block2.size())
            block3 = self.block3(block2)
            # print(block2.size())
            block4 = self.block4(block3)
            # print(block2.size())
            block5 = self.block5(block4)
            # print(block2.size())
            block6 = self.block6(block5)
            # print(block2.size())
            block7 = self.block7(block6)
            # block7 = self.se7(block7, q)
            # print(block2.size())
            block8 = self.block8(block1 + block7)
        else:
            block1 = self.block1(x)
            block2 = self.block2(block1)
            # print(block2.size())
            block2 = self.se2(block2, q)
            # print(block2.size())
            block3 = self.block3(block2)
            block3 = self.se3(block3, q)
            # print(block2.size())
            block4 = self.block4(block3)
            block4 = self.se4(block4, q)
            # print(block2.size())
            block5 = self.block5(block4)
            block5 = self.se5(block5, q)
            # print(block2.size())
            block6 = self.block6(block5)
            block6 = self.se6(block6, q)
            # print(block2.size())
            block7 = self.block7(block6)
            # block7 = self.se7(block7, q)
            # print(block2.size())
            block8 = self.block8(block1 + block7)
        # return block8
        return (F.tanh(block8) + 1) / 2


class BidirectionalLSTM(nn.Module):

    def __init__(self, nIn, nHidden, nOut):
        super(BidirectionalLSTM, self).__init__()

        self.rnn = nn.LSTM(nIn, nHidden, bidirectional=True)
        self.embedding = nn.Linear(nHidden * 2, nOut)

    def forward(self, input):
        recurrent, _ = self.rnn(input)
        T, b, h = recurrent.size()
        t_rec = recurrent.view(T * b, h)

        output = self.embedding(t_rec)  # [T * b, nOut]
        output = output.view(T, b, -1)

        return output


class Dis_crnn(nn.Module):

    def __init__(self, imgH, nc, nclass, nh=256, n_rnn=2, leakyRelu=False):
        super(Dis_crnn, self).__init__()
        assert imgH % 16 == 0, 'imgH has to be a multiple of 16'
        ks = [3, 3, 3, 3, 3, 3, (2, 3)]
        ps = [1, 1, 1, 1, 1, 1, (0, 1)]
        ss = [1, 1, 1, 1, 1, 1, (2, 1)]
        nm = [64, 128, 256, 256, 512, 512, 512]

        cnn = nn.Sequential()

        def convRelu(i, batchNormalization=False):
            nIn = nc if i == 0 else nm[i - 1]
            nOut = nm[i]
            cnn.add_module('conv{0}'.format(i),
                           nn.Conv2d(nIn, nOut, ks[i], ss[i], ps[i]))
            if batchNormalization:
                cnn.add_module('batchnorm{0}'.format(i), nn.BatchNorm2d(nOut))
            if leakyRelu:
                cnn.add_module('relu{0}'.format(i),
                               nn.LeakyReLU(0.2, inplace=True))
            else:
                cnn.add_module('relu{0}'.format(i), nn.ReLU(True))

        convRelu(0)
        cnn.add_module('pooling{0}'.format(0), nn.MaxPool2d(2, 2))  # 64x16x64
        convRelu(1)
        cnn.add_module('pooling{0}'.format(1), nn.MaxPool2d(2, 2))  # 128x8x32
        convRelu(2, True)
        convRelu(3)
        cnn.add_module('pooling{0}'.format(2),
                       nn.MaxPool2d((2, 3), (2, 1), (0, 1)))  # 256x4x16
        convRelu(4, True)
        convRelu(5)
        cnn.add_module('pooling{0}'.format(3),
                       nn.MaxPool2d((2, 3), (2, 1), (0, 1)))  # 512x2x16
        convRelu(6, True)  # 512x1x16
        # convRelu(7)
        # cnn.add_module('pooling{0}'.format(5),
        #                nn.MaxPool2d((2, 2), (2, 1), (0, 1)))
        # convRelu(8, True)

        self.cnn = cnn
        self.rnn = nn.Sequential(
            BidirectionalLSTM(512, nh, nh),
            BidirectionalLSTM(nh, nh, nclass))

        self.dis = nn.Sequential(nn.AdaptiveAvgPool2d(1),
                                 nn.Conv2d(512, 1024, kernel_size=1),
                                 nn.LeakyReLU(0.2),
                                 nn.Conv2d(1024, 1, kernel_size=1))

    def forward(self, src_tokens):
        batch_size = src_tokens.size(0)
        conv = self.cnn(src_tokens)
        b, c, h, w = conv.size()
        q = conv
        # print(q.size())

        # print(conv.size())
        # assert h == 1, "the height of conv must be 1"
        dis_prob = self.dis(conv)
        dis_prob = F.sigmoid(dis_prob.view(batch_size))
        # print(dis_prob.size())

        conv = conv.squeeze(2)
        conv = conv.permute(2, 0, 1)  # [w, b, c]
        # rnn features
        output = self.rnn(conv)
        return dis_prob, output, q


class Discriminator(nn.Module):
    def __init__(self):
        super(Discriminator, self).__init__()
        self.net = nn.Sequential(
            nn.Conv2d(1, 64, kernel_size=3, padding=1),
            nn.LeakyReLU(0.2),

            nn.Conv2d(64, 64, kernel_size=3, stride=2, padding=1),
            nn.BatchNorm2d(64),
            nn.LeakyReLU(0.2),

            nn.Conv2d(64, 128, kernel_size=3, padding=1),
            nn.BatchNorm2d(128),
            nn.LeakyReLU(0.2),

            nn.Conv2d(128, 128, kernel_size=3, stride=2, padding=1),
            nn.BatchNorm2d(128),
            nn.LeakyReLU(0.2),

            nn.Conv2d(128, 256, kernel_size=3, padding=1),
            nn.BatchNorm2d(256),
            nn.LeakyReLU(0.2),

            nn.Conv2d(256, 256, kernel_size=3, stride=2, padding=1),
            nn.BatchNorm2d(256),
            nn.LeakyReLU(0.2),

            nn.Conv2d(256, 512, kernel_size=3, padding=1),
            nn.BatchNorm2d(512),
            nn.LeakyReLU(0.2),

            nn.Conv2d(512, 512, kernel_size=3, stride=2, padding=1),
            nn.BatchNorm2d(512),
            nn.LeakyReLU(0.2),

            nn.AdaptiveAvgPool2d(1),
            nn.Conv2d(512, 1024, kernel_size=1),
            nn.LeakyReLU(0.2),
            nn.Conv2d(1024, 1, kernel_size=1)
        )

    def forward(self, x):
        batch_size = x.size(0)
        # return self.net(x)
        return F.sigmoid(self.net(x).view(batch_size))


class ResidualBlock(nn.Module):
    def __init__(self, channels):
        super(ResidualBlock, self).__init__()
        self.conv1 = nn.Conv2d(channels, channels, kernel_size=3, padding=1)
        self.bn1 = nn.BatchNorm2d(channels)
        self.prelu = nn.PReLU()
        self.conv2 = nn.Conv2d(channels, channels, kernel_size=3, padding=1)
        self.bn2 = nn.BatchNorm2d(channels)

    def forward(self, x):
        residual = self.conv1(x)
        residual = self.bn1(residual)
        residual = self.prelu(residual)
        residual = self.conv2(residual)
        residual = self.bn2(residual)

        return x + residual


class UpsampleBLock(nn.Module):
    def __init__(self, in_channels, up_scale):
        super(UpsampleBLock, self).__init__()
        self.conv = nn.Conv2d(in_channels, in_channels * up_scale ** 2, kernel_size=3, padding=1)
        self.pixel_shuffle = nn.PixelShuffle(up_scale)
        self.prelu = nn.PReLU()

    def forward(self, x):
        x = self.conv(x)
        x = self.pixel_shuffle(x)
        x = self.prelu(x)
        return x


# def Linear(in_features, out_features, dropout=0):
#     """Weight-normalized Linear layer (input: N x T x C)"""
#     m = nn.Linear(in_features, out_features)
#     nn.init.normal_(m.weight, mean=0, std=math.sqrt((1 - dropout) / in_features))
#     nn.init.constant_(m.bias, 0)
#     return nn.utils.weight_norm(m)
#
# class AttentionLayer(nn.Module):
#     def __init__(self, conv_channels, embed_dim, normalization_constant=0.5, bmm=None):
#         super().__init__()
#         self.normalization_constant = normalization_constant
#         # projects from output of convolution to embedding dimension
#         self.in_projection = Linear(conv_channels, embed_dim)
#         # projects from embedding dimension to convolution size
#         self.out_projection = Linear(embed_dim, conv_channels)
#
#         self.bmm = bmm if bmm is not None else torch.bmm
#
#     def forward(self, x, target_embedding, encoder_out, encoder_padding_mask):
#         residual = x
#
#         # attention
#         x = (self.in_projection(x) + target_embedding) * math.sqrt(self.normalization_constant)
#         #key = F.normalize(encoder_out[0], dim=1)
#         #x = self.bmm(x, key)
#         x = self.bmm(x, encoder_out[0])
#
#         # don't attend over padding
#         if encoder_padding_mask is not None:
#             x = x.float().masked_fill(
#                 encoder_padding_mask.unsqueeze(1),
#                 float('-inf')
#             ).type_as(x)  # FP16 support: cast to float and back
#
#         # softmax over last dim
#         sz = x.size()
#         x = F.softmax(x.view(sz[0] * sz[1], sz[2]), dim=1)
#         x = x.view(sz)
#         attn_scores = x
#
#         x = self.bmm(x, encoder_out[1])
#
#         # scale attention output (respecting potentially different lengths)
#         s = encoder_out[1].size(1)
#         if encoder_padding_mask is None:
#             x = x * (s * math.sqrt(1.0 / s))
#         else:
#             s = s - encoder_padding_mask.type_as(x).sum(dim=1, keepdim=True)  # exclude padding
#             s = s.unsqueeze(-1)
#             x = x * (s * s.rsqrt())
#
#         # project back
#         x = (self.out_projection(x) + residual) * math.sqrt(self.normalization_constant)
#         return x, attn_scores
#
#     def make_generation_fast_(self, beamable_mm_beam_size=None, **kwargs):
#         """Replace torch.bmm with BeamableMM."""
#         if beamable_mm_beam_size is not None:
#             del self.bmm
#             self.add_module('bmm', BeamableMM(beamable_mm_beam_size))

if __name__ == "__main__":
    G = Generator(4)
    D = Dis_crnn(imgH=64, nc=3, nclass=36, nh=512, n_rnn=2, leakyRelu=False)
    # model = ModelAll(4, imgH=64, nc=3, nclass=36, nh=512, n_rnn=2, leakyRelu=False)
    # D2 = Discriminator()
    img = torch.randn(4, 3, 32, 100)
    img2 = torch.randn(4, 3, 8, 25)
    # # out2 = D2(img)
    dis_prob, out, q = D(img)
    print(dis_prob.size(), out.size(), q.size())
    out = G(img2, q)
    print(out.size())