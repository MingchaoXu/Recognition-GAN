python -u train_reg_v2.py --device_id 2 \
 --crop_size 32 \
 --config cfg/config-v2-1,1.yaml \
 --fine_G ../epochs/v2-1,1/netG_epoch_4_2.pth \
  --fine_D ../epochs/v2-1,1/netD_epoch_4_2.pth \
 --test \
 --generate \
#  --fine_D ../finetune-model/checkpoint130.pt
