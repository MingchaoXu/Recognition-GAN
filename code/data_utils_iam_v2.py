import torch
import os
import random
import cv2
import numpy as np
from os import listdir
from os.path import join
import lmdb
import lz4.block
import struct

from PIL import Image
from torch.utils.data.dataset import Dataset
from torchvision.transforms import Compose, RandomCrop, ToTensor, ToPILImage, CenterCrop, Resize

# iam 数据集复现陈卓师兄mdlstm结构，使用lmdb数据集



def is_image_file(filename):
    return any(filename.endswith(extension) for extension in ['.png', '.jpg', '.jpeg', '.PNG', '.JPG', '.JPEG', '.pgm'])


def calculate_valid_crop_size(crop_size, upscale_factor):
    return crop_size - (crop_size % upscale_factor)


def train_hr_transform(crop_size):
    return Compose([
        ToTensor(),
    ])


def train_lr_transform(crop_size, upscale_factor):
    return Compose([
        ToTensor(),
    ])


def display_transform(w, h):
    new_h = 32
    rate = w / h
    new_w = int(new_h * rate)
    return Compose([
        ToPILImage(),
        # Resize((new_h, new_w)),
        # CenterCrop(400),
        ToTensor()
    ])





class TrainDatasetFromFolder2(Dataset):
    # for the dataset /data3/cwang/scene_text_recognition/90kDICT32px/
    def __init__(self, lmdb_path, crop_size, upscale_factor, shuffle=False, h_norm=72):
        super(TrainDatasetFromFolder2, self).__init__()
        self.h_norm = h_norm
        self.upscale_factor = upscale_factor
        # self.image_filenames = []
        self.shuffle = shuffle

        self.lmdb_path = lmdb_path
        # 存放lmdb数据的标签，为了与lua兼容，第一行标签为1
        env = lmdb.Environment(self.lmdb_path)
        self.txn = env.begin()

        self.label_dict = {}
        crop_size = calculate_valid_crop_size(crop_size, upscale_factor)
        self.hr_transform = train_hr_transform(crop_size)
        self.lr_transform = train_lr_transform(crop_size, upscale_factor)
        self.src_sizes = self.src_sizes()
        self.label_list = []

    def __getitem__(self, index, height_norm_flag=True, h_norm=64, w_max=2400):
        # 从1开始
        index += 1
        image_key = 'image-{:09d}'.format(index)
        size_key = 'size-{:09d}'.format(index)
        label_key = 'label-{:09d}'.format(index)
        try:
            size_buf = self.txn.get(size_key.encode('utf-8')).decode('utf-8').split(',')
            image_buf = self.txn.get(image_key.encode('utf-8'))
            label_buf = self.txn.get(label_key.encode('utf-8')).decode('utf-8')
        except:
            print(index)
        size_buf = [int(i) for i in size_buf]
        assert len(size_buf) == 2, 'image size is wrong!'
        image_size = size_buf[0] * size_buf[1]
        # print(image_size)
        image = lz4.block.decompress(image_buf, image_size)
        image = struct.unpack(str(image_size) + 'c', image)
        image = list(map(ord, image))
        ori_image = np.array(image, dtype=np.float32).reshape(size_buf)
        
        ori_image = ori_image*1.0
        ori_image = cv2.resize(ori_image, (1000, 72))
        ori_image = ori_image[:, :, np.newaxis]
        # cv_image = 255 - cv_image
        img = torch.from_numpy(ori_image)  # 现在是H，W，C
        img = torch.transpose(torch.transpose(img, 0, 1), 0, 2)  # 现在是C，H，W
        label_value = label_buf.split(',')
        label_value = list(map(int, label_value))
        # label_value.append(self.src_dict.eos())
        # label_value = [int(i) for i in label_value]
        # print(label_value)
        self.label_dict[str(index)] = label_value
        label = torch.FloatTensor(label_value)
        # 先从0-1到0-255，方便之后opencv resize
        # img = img * 255
        hr_image = img
        lr_image = img
        hr_restore_img = img


        return {
            'id': index,
            'lr_image': lr_image,
            'hr_image': hr_image,
            'hr_restore_img': hr_restore_img,
            'target': label,
        }
        # return lr_image, hr_image

    def __len__(self):
        return int(self.txn.get('num-samples'.encode('utf-8')).decode('utf-8'))

    def collater(self, samples):
        """Merge a list of samples to form a mini-batch."""
        return collate_chenz(
            samples, img_pad_value=0, pad_idx=0, upscale_factor=self.upscale_factor, h_norm=self.h_norm
        )

    def ordered_indices(self):
        """Ordered indices for batching."""
        if self.shuffle:
            data_len = len(self)
            batch_num = data_len // self.batch_size
            bt_list = random.sample(list(range(data_len)), batch_num)
            indices_list = []
            for i in bt_list:
                if i > data_len - self.batch_size:
                    tmp_list = list(range(data_len - self.batch_size, data_len))
                else:
                    tmp_list = list(range(i, i + self.batch_size))
                indices_list += tmp_list
            indices = np.array(indices_list)
            # indices = np.random.permutation(len(self))

        else:
            indices = np.arange(len(self))
        # if self.tgt_sizes is not None:
        #     indices = indices[np.argsort(self.tgt_sizes[indices], kind='mergesort')]
        # 返回的是一个dnarry，里面的元素是句子的id，已经做过乱序，并且按照句子的长度排序
        # return indices[np.argsort(self.src_sizes[indices], kind='mergesort')]
        return indices

    def num_tokens(self, index):
        """Return an example's length (number of tokens), used for batching."""
        return self.src_sizes[index]

    def src_sizes(self):
        # 返回一个ndarray， 每个元素的大小是对应句子的长度
        src_sizes = []
        for i in range(0, len(self)):
            src_sizes.append(len(self[i]['target']))
        return np.array(src_sizes)


class ValDatasetFromFolder2(Dataset):
    # for the dataset /data3/cwang/scene_text_recognition/90kDICT32px/
    def __init__(self, lmdb_path, crop_size, upscale_factor, shuffle=False, h_norm=72):
        super(ValDatasetFromFolder2, self).__init__()
        self.h_norm = h_norm
        self.upscale_factor = upscale_factor
        # self.image_filenames = []
        self.shuffle = shuffle

        self.lmdb_path = lmdb_path
        # 存放lmdb数据的标签，为了与lua兼容，第一行标签为1
        env = lmdb.Environment(self.lmdb_path)
        self.txn = env.begin()

        self.label_dict = {}
        crop_size = calculate_valid_crop_size(crop_size, upscale_factor)
        self.hr_transform = train_hr_transform(crop_size)
        self.lr_transform = train_lr_transform(crop_size, upscale_factor)
        self.src_sizes = self.src_sizes()
        self.label_list = []

    def __getitem__(self, index, height_norm_flag=True, h_norm=64, w_max=2400):
        index += 1
        image_key = 'image-{:09d}'.format(index)
        size_key = 'size-{:09d}'.format(index)
        label_key = 'label-{:09d}'.format(index)
        try:
            size_buf = self.txn.get(size_key.encode('utf-8')).decode('utf-8').split(',')
            image_buf = self.txn.get(image_key.encode('utf-8'))
            label_buf = self.txn.get(label_key.encode('utf-8')).decode('utf-8')
        except:
            print(index)
        size_buf = [int(i) for i in size_buf]
        assert len(size_buf) == 2, 'image size is wrong!'
        image_size = size_buf[0] * size_buf[1]
        # print(image_size)
        image = lz4.block.decompress(image_buf, image_size)
        image = struct.unpack(str(image_size) + 'c', image)
        image = list(map(ord, image))
        ori_image = np.array(image, dtype=np.float32).reshape(size_buf)
        # tsne 要定长
        ori_image = ori_image*1.0
        ori_image = cv2.resize(ori_image, (1000, 72))
        ori_image = ori_image[:, :, np.newaxis]
        # cv_image = 255 - cv_image
        img = torch.from_numpy(ori_image)  # 现在是H，W，C
        img = torch.transpose(torch.transpose(img, 0, 1), 0, 2)  # 现在是C，H，W
        label_value = label_buf.split(',')
        label_value = list(map(int, label_value))
        # label_value.append(self.src_dict.eos())
        # label_value = [int(i) for i in label_value]
        # print(label_value)
        self.label_dict[str(index)] = label_value
        label = torch.FloatTensor(label_value)
        # 先从0-1到0-255，方便之后opencv resize
        # img = img * 255
        ori_image = img
        hr_image = img
        lr_image = img
        hr_restore_img = img

        return {
            'id': index,
            'ori_image': ori_image,
            'lr_image': lr_image,
            'hr_restore_img': hr_restore_img,
            'hr_image': hr_image,
            'target': label,
        }
        # return ToTensor()(lr_image), ToTensor()(hr_restore_img), ToTensor()(hr_image)

    def __len__(self):
        return int(self.txn.get('num-samples'.encode('utf-8')).decode('utf-8'))

    def collater(self, samples):
        """Merge a list of samples to form a mini-batch."""
        return collate_chenz_val(
            samples, img_pad_value=0, pad_idx=0, upscale_factor=self.upscale_factor, h_norm=self.h_norm
        )
        # return collate_val(
        #     samples, img_pad_value=-1, pad_idx=0, upscale_factor=self.upscale_factor
        # )
    def src_sizes(self):
        # 返回一个ndarray， 每个元素的大小是对应句子的长度
        src_sizes = []
        for i in range(0, len(self)):
            src_sizes.append(len(self[i]['target']))
        return np.array(src_sizes)


class ValDatasetFromFolder_generate(Dataset):
    # for the dataset /data3/cwang/scene_text_recognition/90kDICT32px/
    def __init__(self, lmdb_path, crop_size, upscale_factor, shuffle=False, h_norm=72):
        super(ValDatasetFromFolder_generate, self).__init__()
        # import pdb; pdb.set_trace()
        self.h_norm = h_norm
        self.upscale_factor = upscale_factor
        # self.image_filenames = []
        self.shuffle = shuffle

        self.lmdb_path = lmdb_path
        # 存放lmdb数据的标签，为了与lua兼容，第一行标签为1
        env = lmdb.Environment(self.lmdb_path)
        self.txn = env.begin()

        self.label_dict = {}
        crop_size = calculate_valid_crop_size(crop_size, upscale_factor)
        self.hr_transform = train_hr_transform(crop_size)
        self.lr_transform = train_lr_transform(crop_size, upscale_factor)
        self.src_sizes = self.src_sizes()
        self.label_list = []

    def __getitem__(self, index, height_norm_flag=True, h_norm=64, w_max=2400):
        index += 1
        image_key = 'image-{:09d}'.format(index)
        size_key = 'size-{:09d}'.format(index)
        label_key = 'label-{:09d}'.format(index)
        try:
            size_buf = self.txn.get(size_key.encode('utf-8')).decode('utf-8').split(',')
            image_buf = self.txn.get(image_key.encode('utf-8'))
            label_buf = self.txn.get(label_key.encode('utf-8')).decode('utf-8')
        except:
            print(index)
        size_buf = [int(i) for i in size_buf]
        assert len(size_buf) == 2, 'image size is wrong!'
        image_size = size_buf[0] * size_buf[1]
        # print(image_size)
        image = lz4.block.decompress(image_buf, image_size)
        image = struct.unpack(str(image_size) + 'c', image)
        image = list(map(ord, image))
        ori_image = np.array(image, dtype=np.float32).reshape(size_buf)

        # tsne 要定长
        ori_image = ori_image*1.0
        ori_image = cv2.resize(ori_image, (1000, 72))
        ori_image = ori_image[:, :, np.newaxis]

        # ori_image = ori_image[:, :, np.newaxis]

        # cv_image = 255 - cv_image
        img = torch.from_numpy(ori_image)  # 现在是H，W，C
        img = torch.transpose(torch.transpose(img, 0, 1), 0, 2)  # 现在是C，H，W
        label_value = label_buf.split(',')
        label_value = list(map(int, label_value))
        # label_value.append(self.src_dict.eos())
        # label_value = [int(i) for i in label_value]
        # print(label_value)
        self.label_dict[str(index)] = label_value
        label = torch.FloatTensor(label_value)
        # 先从0-1到0-255，方便之后opencv resize
        # img = img * 255
        ori_image = img
        hr_image = img
        lr_image = img
        hr_restore_img = img





        return {
            'id': index,
            'ori_image': ori_image,
            'lr_image': lr_image,
            'hr_restore_img': hr_restore_img,
            'hr_image': hr_image,
            'target': label,
        }
        # return ToTensor()(lr_image), ToTensor()(hr_restore_img), ToTensor()(hr_image)

    def __len__(self):
        return int(self.txn.get('num-samples'.encode('utf-8')).decode('utf-8'))

    def collater(self, samples):
        """Merge a list of samples to form a mini-batch."""
        return collate_chenz_val(
            samples, img_pad_value=0, pad_idx=0, upscale_factor=self.upscale_factor, h_norm=self.h_norm
        )
    def src_sizes(self):
        # 返回一个ndarray， 每个元素的大小是对应句子的长度
        src_sizes = []
        for i in range(0, len(self)):
            src_sizes.append(len(self[i]['target']))
        return np.array(src_sizes)

class ValDatasetFromFolder_generate2(Dataset):
    # for the dataset 自己制作的数据集
    def __init__(self, dataset_dir, source_file, str2label, label2str,  str2label_2,upscale_factor, h_norm=128):
        super(ValDatasetFromFolder_generate2, self).__init__()
        self.h_norm = h_norm
        self.upscale_factor = upscale_factor
        self.source_file = source_file
        self.image_filenames = []
        self.dataset_dir = dataset_dir
        with open(self.source_file, 'r') as fin:
            for i in fin.readlines():
                i = i.rstrip()


                folder, name = os.path.split(i)
                # black list
                # import pdb; pdb.set_trace()
                name = os.path.splitext(name)[0][:-6]
                gt_name = os.path.join(self.dataset_dir, folder, name+'_gt.txt')
                with open(gt_name, 'r') as f:
                    gt = f.readline()
                    gt = gt.rsplit('\n')[0]
                    gt = gt.split(' ')
                    gt = [int(i1) for i1 in gt]
                # name_list = name.split('_')
                
                label_list = gt
                label_str = [label2str[str(lb)] for lb in label_list]
                label_list = [str2label_2[lb_str] for lb_str in label_str]

                self.image_filenames.append((i, label_list))
        
        # self.image_filenames = [join(dataset_dir, x) for x in listdir(dataset_dir) if is_image_file(x)]

    def __getitem__(self, index, height_norm_flag=True, h_norm=64, w_max=2400):
        img_path, label_list = self.image_filenames[index]
        # print(self.image_filenames[0])
        img_path = join(self.dataset_dir, img_path)
        label = torch.FloatTensor(label_list)
        # ori_image = Image.open(img_path)
        ori_image = np.array(cv2.imread(img_path, cv2.IMREAD_GRAYSCALE))
        ori_image = ori_image*1.0
        ori_image = cv2.resize(ori_image, (1000, 72))
        ori_image = ori_image[:, :, np.newaxis]

        # cv_image = 255 - cv_image
        img = torch.from_numpy(ori_image).float() # 现在是H，W，C
        img = torch.transpose(torch.transpose(img, 0, 1), 0, 2)  # 现在是C，H，W
        # 先从0-1到0-255，方便之后opencv resize
        # img = img * 255
        img = 255-img
        ori_image = img
        hr_image = img
        lr_image = img
        hr_restore_img = img
        return {
            'id': index,
            'ori_image': ori_image,
            'lr_image': lr_image,
            'hr_restore_img': hr_restore_img,
            'hr_image': hr_image,
            'target': label,
        }
        # return ToTensor()(lr_image), ToTensor()(hr_restore_img), ToTensor()(hr_image)

    def __len__(self):
        return len(self.image_filenames)

    def collater(self, samples):
        """Merge a list of samples to form a mini-batch."""
        return collate_chenz_val(
            samples, img_pad_value=0, pad_idx=0, upscale_factor=self.upscale_factor, h_norm=self.h_norm
        )


def collate(samples, img_pad_value, pad_idx, upscale_factor=4, left_pad_source=True, left_pad_target=False,
            ctc_flag=False):
    if len(samples) == 0:
        return {}

    def merge(key, left_pad, move_eos_to_beginning=False, ctc_flag=False, sort_order=None,
              upscale_factor=upscale_factor):
        # source 是图像，要不同的处理
        if key == 'lr_image' or key == 'hr_image' or key == 'hr_restore_img':
            # img_list = []
            # img_list.append(collate_tokens([s[key] for s in samples], img_pad_value))
            imgs = collate_tokens2([s[key] for s in samples], img_pad_value, upscale_factor)
            # hr_image = collate_tokens([s[1] for s in samples], img_pad_value)
            # [s[key] for s in samples],
            return imgs
            # return (lr_image, hr_image)
        else:
            # 文本数据
            return crnn_collate_tokens(
                [s[key] for s in samples],
                pad_idx, left_pad, move_eos_to_beginning, ctc_flag=ctc_flag, sort_order=sort_order
            )

    id = torch.LongTensor([s['id'] for s in samples])
    lr_image = merge('lr_image', left_pad=left_pad_source)
    hr_image = merge('hr_image', left_pad=left_pad_source)
    hr_restore_img = merge('hr_restore_img', left_pad=left_pad_source)
    # sort by descending source length
    # src_lengths 存放的是每个sample的图片的长度， C,H,W
    src_lengths = torch.LongTensor([s['hr_image'].size(2) for s in samples])
    src_lengths, sort_order = src_lengths.sort(descending=False)
    id = id.index_select(0, sort_order)
    lr_image = lr_image.index_select(0, sort_order)
    hr_image = hr_image.index_select(0, sort_order)
    hr_restore_img = hr_restore_img.index_select(0, sort_order)

    if samples[0].get('target', None) is not None:
        # if not ctc_flag:
        target = merge('target', left_pad=left_pad_target)
        # we create a shifted version of targets for feeding the
        # previous output token(s) into the next decoder step
        prev_output_tokens = merge(
            'target',
            left_pad=left_pad_target,
            move_eos_to_beginning=True,
        )
        # prev_output_tokens = prev_output_tokens.index_select(0, sort_order)
        # target = target.index_select(0, sort_order).long()
        # ntokens = sum(len(s['target']) for s in samples)
        # else:
        target_before_ctc = merge('target', left_pad=left_pad_target)
        target_before_ctc = target_before_ctc.index_select(0, sort_order).long()
        target_ctc = merge('target', left_pad=left_pad_target, ctc_flag=True, sort_order=sort_order)
        # ntokens_ctc = torch.IntTensor([len(s['target']) for s in samples]).numpy()
        ntokens_ctc = torch.IntTensor([len(s['target']) for s in samples]).index_select(0, sort_order).numpy()
    else:
        ntokens = sum(s['source'].size(2) for s in samples)

    return {
        'id': id,
        'lr_image': lr_image,
        'hr_image': hr_image,
        'hr_restore_img': hr_restore_img,
        'target': target_ctc,
        'ntokens': ntokens_ctc,
        'target_before': target_before_ctc
    }
    # return src_tokens


def collate_val(samples, img_pad_value, pad_idx, upscale_factor=4, left_pad_source=True, left_pad_target=False,
                ctc_flag=False, ):
    if len(samples) == 0:
        return {}

    def merge(key, left_pad, move_eos_to_beginning=False, ctc_flag=False, sort_order=None):
        # source 是图像，要不同的处理
        if key == 'lr_image' or key == 'hr_image' or key == 'hr_restore_img':
            # img_list = []
            # img_list.append(collate_tokens([s[key] for s in samples], img_pad_value))
            imgs = collate_tokens2([s[key] for s in samples], img_pad_value, upscale_factor)
            # hr_image = collate_tokens([s[1] for s in samples], img_pad_value)
            # [s[key] for s in samples],
            return imgs
            # return (lr_image, hr_image)
        elif key == 'ori_image':
            img_list = []
            # print(samples)
            # for s in samples:
            # print('ori_image', s[key].size())
            img_list = [s[key] for s in samples]
            # print('img_list:', len(img_list))
            return img_list
        else:
            # 文本数据
            return crnn_collate_tokens(
                [s[key] for s in samples],
                pad_idx, left_pad, move_eos_to_beginning, ctc_flag=ctc_flag, sort_order=sort_order
            )

    id = torch.LongTensor([s['id'] for s in samples])
    lr_image = merge('lr_image', left_pad=left_pad_source)
    hr_image = merge('hr_image', left_pad=left_pad_source)
    hr_restore_img = merge('hr_restore_img', left_pad=left_pad_source)
    ori_image_tmp = merge('ori_image', left_pad=left_pad_source)
    # print('ori_image_tmp', len(ori_image_tmp))

    # sort by descending source length
    # src_lengths 存放的是每个sample的图片的长度， C,H,W
    src_lengths = torch.LongTensor([s['hr_image'].size(2) for s in samples])
    src_lengths, sort_order = src_lengths.sort(descending=False)
    id = id.index_select(0, sort_order)
    lr_image = lr_image.index_select(0, sort_order)
    hr_image = hr_image.index_select(0, sort_order)
    hr_restore_img = hr_restore_img.index_select(0, sort_order)
    ori_image = []
    # print(len(ori_image_tmp), sort_order.size(), sort_order)
    for i in range(sort_order.size()[0]):
        # print(ori_image_tmp[sort_order[i]].size(), ori_image_tmp[0].size(), sort_order[i])
        ori_image.append(ori_image_tmp[sort_order[i]])
    # print(ori_image[0].size())
    if samples[0].get('target', None) is not None:
        # if not ctc_flag:
        target = merge('target', left_pad=left_pad_target)
        # we create a shifted version of targets for feeding the
        # previous output token(s) into the next decoder step
        prev_output_tokens = merge(
            'target',
            left_pad=left_pad_target,
            move_eos_to_beginning=True,
        )
        # prev_output_tokens = prev_output_tokens.index_select(0, sort_order)
        # target = target.index_select(0, sort_order).long()
        # ntokens = sum(len(s['target']) for s in samples)
        # else:
        target_before_ctc = merge('target', left_pad=left_pad_target)
        target_before_ctc = target_before_ctc.index_select(0, sort_order).long()
        target_ctc = merge('target', left_pad=left_pad_target, ctc_flag=True, sort_order=sort_order)
        # ntokens_ctc = torch.IntTensor([len(s['target']) for s in samples]).numpy()
        ntokens_ctc = torch.IntTensor([len(s['target']) for s in samples]).index_select(0, sort_order).numpy()
    else:
        ntokens = sum(s['source'].size(2) for s in samples)
    # print(ori_image)
    return {
        'id': id,
        'ori_image': ori_image,
        'lr_image': lr_image,
        'hr_image': hr_image,
        'hr_restore_img': hr_restore_img,
        'target': target_ctc,
        'ntokens': ntokens_ctc,
        'target_before': target_before_ctc
    }

def collate_chenz(samples, img_pad_value, pad_idx, upscale_factor=4, left_pad_source=True, left_pad_target=False, h_norm=72,
            ctc_flag=False):
    if len(samples) == 0:
        return {}

    def merge(key, left_pad, move_eos_to_beginning=False, ctc_flag=False, sort_order=None,
              upscale_factor=upscale_factor, h_norm=h_norm):
        # source 是图像，要不同的处理
        # import pdb; pdb.set_trace()
        if key == 'all_image':
            # img_list = []
            # img_list.append(collate_tokens([s[key] for s in samples], img_pad_value))
            img_list = [s['hr_image'] for s in samples]
            hr_image, lr_image, hr_restore_img = collate_tokens_chenz(img_list, img_pad_value, upscale_factor, h_norm)
            # hr_image = collate_tokens([s[1] for s in samples], img_pad_value)
            # [s[key] for s in samples],
            return hr_image, lr_image, hr_restore_img
            # return (lr_image, hr_image)
        else:
            # 文本数据
            return crnn_collate_tokens(
                [s[key] for s in samples],
                pad_idx, left_pad, move_eos_to_beginning, ctc_flag=ctc_flag, sort_order=sort_order
            )

    id = torch.LongTensor([s['id'] for s in samples])
    hr_image, lr_image, hr_restore_img = merge('all_image', left_pad=left_pad_source)
    # lr_image = merge('lr_image', left_pad=left_pad_source)
    #     # hr_image = merge('hr_image', left_pad=left_pad_source)
    #     # hr_restore_img = merge('hr_restore_img', left_pad=left_pad_source)
    # sort by descending source length
    # src_lengths 存放的是每个sample的图片的长度， C,H,W
    src_lengths = torch.LongTensor([s['hr_image'].size(2) for s in samples])
    src_lengths, sort_order = src_lengths.sort(descending=False)
    id = id.index_select(0, sort_order)
    lr_image = lr_image.index_select(0, sort_order)
    hr_image = hr_image.index_select(0, sort_order)
    hr_restore_img = hr_restore_img.index_select(0, sort_order)

    if samples[0].get('target', None) is not None:
        # if not ctc_flag:
        target = merge('target', left_pad=left_pad_target)
        # we create a shifted version of targets for feeding the
        # previous output token(s) into the next decoder step
        prev_output_tokens = merge(
            'target',
            left_pad=left_pad_target,
            move_eos_to_beginning=True,
        )
        # prev_output_tokens = prev_output_tokens.index_select(0, sort_order)
        # target = target.index_select(0, sort_order).long()
        # ntokens = sum(len(s['target']) for s in samples)
        # else:
        target_before_ctc = merge('target', left_pad=left_pad_target)
        target_before_ctc = target_before_ctc.index_select(0, sort_order).long()
        target_ctc = merge('target', left_pad=left_pad_target, ctc_flag=True, sort_order=sort_order)
        # ntokens_ctc = torch.IntTensor([len(s['target']) for s in samples]).numpy()
        ntokens_ctc = torch.IntTensor([len(s['target']) for s in samples]).index_select(0, sort_order).numpy()
    else:
        ntokens = sum(s['source'].size(2) for s in samples)

    return {
        'id': id,
        'lr_image': lr_image,
        'hr_image': hr_image,
        'hr_restore_img': hr_restore_img,
        'target': target_ctc,
        'ntokens': ntokens_ctc,
        'target_before': target_before_ctc
    }
    # return src_tokens


def collate_chenz_val(samples, img_pad_value, pad_idx, upscale_factor=4, left_pad_source=True, left_pad_target=False, h_norm=72,
            ctc_flag=False):
    if len(samples) == 0:
        return {}

    def merge(key, left_pad, move_eos_to_beginning=False, ctc_flag=False, sort_order=None,
              upscale_factor=upscale_factor, h_norm=h_norm):
        # source 是图像，要不同的处理
        # import pdb; pdb.set_trace()
        if key == 'all_image':
            # img_list = []
            # img_list.append(collate_tokens([s[key] for s in samples], img_pad_value))
            # import pdb; pdb.set_trace()
            img_list = [s['hr_image'] for s in samples]
            hr_image, lr_image, hr_restore_img = collate_tokens_chenz(img_list, img_pad_value, upscale_factor, h_norm)
            # hr_image = collate_tokens([s[1] for s in samples], img_pad_value)
            # [s[key] for s in samples],
            return hr_image, lr_image, hr_restore_img
            # return (lr_image, hr_image)
        elif key == 'ori_image':
            img_list = []
            # print(samples)
            # for s in samples:
            # print('ori_image', s[key].size())
            img_list = [s[key] for s in samples]
            # print('img_list:', len(img_list))
            return img_list
        else:
            # 文本数据
            return crnn_collate_tokens(
                [s[key] for s in samples],
                pad_idx, left_pad, move_eos_to_beginning, ctc_flag=ctc_flag, sort_order=sort_order
            )

    id = torch.LongTensor([s['id'] for s in samples])
    hr_image, lr_image, hr_restore_img = merge('all_image', left_pad=left_pad_source)
    ori_image_tmp = merge('ori_image', left_pad=left_pad_source)

    # lr_image = merge('lr_image', left_pad=left_pad_source)
    #     # hr_image = merge('hr_image', left_pad=left_pad_source)
    #     # hr_restore_img = merge('hr_restore_img', left_pad=left_pad_source)
    # sort by descending source length
    # src_lengths 存放的是每个sample的图片的长度， C,H,W
    # import pdb; pdb.set_trace()
    src_lengths = torch.LongTensor([s['hr_image'].size(2) for s in samples])
    src_lengths, sort_order = src_lengths.sort(descending=False)
    id = id.index_select(0, sort_order)
    lr_image = lr_image.index_select(0, sort_order)
    hr_image = hr_image.index_select(0, sort_order)
    hr_restore_img = hr_restore_img.index_select(0, sort_order)
    ori_image = []
    # print(len(ori_image_tmp), sort_order.size(), sort_order)
    for i in range(sort_order.size()[0]):
        # print(ori_image_tmp[sort_order[i]].size(), ori_image_tmp[0].size(), sort_order[i])
        ori_image.append(ori_image_tmp[sort_order[i]])
    if samples[0].get('target', None) is not None:
        # if not ctc_flag:
        target = merge('target', left_pad=left_pad_target)
        # we create a shifted version of targets for feeding the
        # previous output token(s) into the next decoder step
        prev_output_tokens = merge(
            'target',
            left_pad=left_pad_target,
            move_eos_to_beginning=True,
        )
        # prev_output_tokens = prev_output_tokens.index_select(0, sort_order)
        # target = target.index_select(0, sort_order).long()
        # ntokens = sum(len(s['target']) for s in samples)
        # else:
        target_before_ctc = merge('target', left_pad=left_pad_target)
        target_before_ctc = target_before_ctc.index_select(0, sort_order).long()
        target_ctc = merge('target', left_pad=left_pad_target, ctc_flag=True, sort_order=sort_order)
        # ntokens_ctc = torch.IntTensor([len(s['target']) for s in samples]).numpy()
        ntokens_ctc = torch.IntTensor([len(s['target']) for s in samples]).index_select(0, sort_order).numpy()
    else:
        ntokens = sum(s['source'].size(2) for s in samples)

    return {
        'id': id,
        'ori_image': ori_image,
        'lr_image': lr_image,
        'hr_image': hr_image,
        'hr_restore_img': hr_restore_img,
        'target': target_ctc,
        'ntokens': ntokens_ctc,
        'target_before': target_before_ctc
    }
    # return src_tokens

def collate_tokens_chenz(hr_imgs, pad_value=0.0, upscale_factor=4, h_norm=72):
    """Convert a list of 3d tensors into a padded 4d tensor. 对图像从中间padding"""

    # 输入opencv
    realWid = 0
    realHei = h_norm

    for i in range(len(hr_imgs)):
        h_size = hr_imgs[i].size()[1]
        w_size = hr_imgs[i].size()[2]
        tmpwid = 0
        if realHei < h_size:
            v = hr_imgs[i].numpy()
            v = np.transpose(v, (1, 2, 0))
            cv_image = cv2.resize(v, (int((realHei / h_size * w_size)), realHei))
            cv_image = cv_image[:, :, np.newaxis]
            img_array = np.transpose(cv_image, (2, 0, 1))
            img = torch.from_numpy(img_array).float()
            hr_imgs[i] = img
            tmpwid = int((realHei / h_size * w_size) + 20)
        else:
            tmpwid = w_size + 20
        if tmpwid > realWid:
            realWid = tmpwid

    if realWid % upscale_factor != 0:
        realWid = int(round(realWid / upscale_factor) * upscale_factor)

    hr_res = hr_imgs[0].new(len(hr_imgs), hr_imgs[0].size(0), realHei, realWid).fill_(pad_value)
    # hr_res = hr_imgs
    def copy_tensor(src, dst):
        # print(dst.size(), src.size())
        assert dst.numel() == src.numel()
        dst.copy_(src)

    for i, v in enumerate(hr_imgs):
        # print(h_size, w_size, v.size(1), v.size(2), v.size())
        h_tmp_half = (realHei - v.size(1)) // 2
        w_tmp_half = (realWid - v.size(2)) // 2
        # print(h_tmp_half, v.size(1))
        copy_tensor(v, hr_res[i][:, h_tmp_half:h_tmp_half + v.size(1), w_tmp_half:w_tmp_half + v.size(2)])

    lr_res = []
    hr_restore_res = []
    for i in range(hr_res.size(0)):
        v = hr_res[i, :, :, :].numpy()
        v = np.transpose(v, (1, 2, 0))
        lr_image = cv2.resize(v, (realWid // upscale_factor, realHei // upscale_factor))
        hr_restore_image = cv2.resize(lr_image, (realWid, realHei))
        lr_image = lr_image[:, :, np.newaxis]
        hr_restore_image = hr_restore_image[:, :, np.newaxis]
        lr_img = np.transpose(lr_image, (2, 0, 1))
        hr_restore_img = np.transpose(hr_restore_image, (2, 0, 1))
        lr_img = lr_img[np.newaxis, :, :, :]
        hr_restore_img = hr_restore_img[np.newaxis, :, :, :]
        lr_img = torch.from_numpy(lr_img).float()
        hr_restore_img = torch.from_numpy(hr_restore_img).float()
        lr_res.append(lr_img)
        hr_restore_res.append(hr_restore_img)

    lr_res = torch.cat(lr_res, 0)
    hr_restore_res = torch.cat(hr_restore_res, 0)
    hr_res = hr_res / 256.0
    lr_res = lr_res / 256.0
    hr_restore_res = hr_restore_res / 256.0
    # cv_image = np.array(hr_res[0], dtype=np.int32).transpose(1, 2, 0)
    # cv2.imwrite('../image_train/test.jpg', cv_image)
    return hr_res, lr_res, hr_restore_res


def crnn_collate_tokens(values, pad_idx, left_pad, move_eos_to_beginning=False, ctc_flag=False, sort_order=None):
    """Convert a list of 1d tensors into a padded 2d tensor."""

    def copy_tensor(src, dst):
        assert dst.numel() == src.numel()
        dst.copy_(src)

    if not ctc_flag:
        size = max(v.size(0) for v in values)
        res = values[0].new(len(values), size).fill_(pad_idx)
        for i, v in enumerate(values):
            copy_tensor(v, res[i][size - len(v):] if left_pad else res[i][:len(v)])
    else:
        size = sum(v.size(0) for v in values)
        res = values[0].new(size).fill_(pad_idx)
        start = 0
        if sort_order is not None:
            for i in sort_order:
                v = values[i]
                copy_tensor(v, res[start:start + len(v)])
                start += len(v)
        else:
            for i, v in enumerate(values):
                copy_tensor(v, res[start:start + len(v)])
                start += len(v)

    return res


def collate_tokens(values, pad_value=0, resize_height=True, height_norm=128):
    """Convert a list of 3d tensors into a padded 4d tensor. 对图像从中间padding"""

    # C,H,W
    h_size = max(v.size(1) for v in values)
    w_size = max(v.size(2) for v in values)
    # 先构造一个新的tensor作为网络输入的tensor， 是一个二维的tensor，（31，128），31个句子，每句128个token
    res = values[0].new(len(values), values[0].size(0), h_size, w_size).fill_(pad_value)

    def copy_tensor(src, dst):
        # print(dst.size(), src.size())
        assert dst.numel() == src.numel()
        dst.copy_(src)

    for i, v in enumerate(values):
        # print(h_size, w_size, v.size(1), v.size(2), v.size())
        h_tmp_half = (h_size - v.size(1)) // 2
        w_tmp_half = (w_size - v.size(2)) // 2
        # print(h_tmp_half, v.size(1))
        copy_tensor(v, res[i][:, h_tmp_half:h_tmp_half + v.size(1), w_tmp_half:w_tmp_half + v.size(2)])
    return res


def collate_tokens2(values, pad_value=255, upscale_factor=4, resize_height=True, height_norm=128):
    """Convert a list of 3d tensors into a padded 4d tensor. 对图像从中间padding"""

    # 输入opencv
    h_size = max(v.size()[1] for v in values)
    w_size = max(v.size()[2] for v in values)
    # if w_size%upscale_factor !=0:
    #     w_size = int(round(w_size/upscale_factor)*upscale_factor)

    res = []
    for i in range(len(values)):
        v = values[i].numpy()
        v = np.transpose(v, (1, 2, 0))
        cv_image = cv2.resize(v, (w_size, h_size))
        cv_image = cv_image[:, :, np.newaxis]
        img_array = np.transpose(cv_image, (2, 0, 1))
        img_array = img_array[np.newaxis, :, :, :]
        img = torch.from_numpy(img_array).float()
        img = img.sub_(0.5).mul_(2.0)
        res += [img]

    res = torch.cat(res, 0)
    return res


def collate_tokens2list(values, pad_value=0, resize_height=True, height_norm=128):
    """Convert a list of 3d tensors into a padded 4d tensor. 对图像从中间padding"""

    # C,H,W
    h_size = max(v.size(1) for v in values)
    w_size = max(v.size(2) for v in values)

    # 先构造一个新的tensor作为网络输入的tensor， 是一个二维的tensor，（31，128），31个句子，每句128个token

    def copy_tensor(src, dst):
        # print(dst.size(), src.size())
        assert dst.numel() == src.numel()
        dst.copy_(src)

    out_list = []
    for i, v in enumerate(values):
        res = values[0].new(values[0].size(0), h_size, w_size).fill_(pad_value)

        # print(h_size, w_size, v.size(1), v.size(2), v.size())
        h_tmp_half = (h_size - v.size(1)) // 2
        w_tmp_half = (w_size - v.size(2)) // 2
        # print(h_tmp_half, v.size(1))
        copy_tensor(v, res[:, h_tmp_half:h_tmp_half + v.size(1), w_tmp_half:w_tmp_half + v.size(2)])
        out_list.append(res)
    return out_list


if __name__ == "__main__":
    lmdb_path = '/data2/mcxu/FormularDatabase/iamLMDB/train'
    # source_file = '/data3/cwang/scene_text_recognition/90kDICT32px/annotation_train.txt'
    # labelstr = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
    # str2label = {}
    # label2str = {}
    # lmdb_path = '/data2/mcxu/FormularDatabase/iamLMDB/train'
    # for idx, char in enumerate(labelstr, start=1):
    #     str2label[char] = idx

    # for key in str2label.keys():
    #     label2str[str(str2label[key])] = key
    crop_size = 72
    upscale_factor = 4
    dataset = TrainDatasetFromFolder2(lmdb_path, crop_size, upscale_factor, shuffle=False, h_norm=128)
    out = dataset[0]