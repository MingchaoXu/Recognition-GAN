import os
import torch
import shutil
import edit_distance
import math
import optim
from optim import lr_scheduler as lr_sched
# from torch.serialization import default_restore_location

def build_optimizer(opt, model):
    optimizer = optim.build_optimizer(opt.optimizerD, opt, model.parameters())
    lr_scheduler = lr_sched.build_lr_scheduler(opt, optimizer)
    return optimizer, lr_scheduler

def load_state(path, model, optimizer=None):
    def map_func(storage, location):
        return storage.cuda()
    if os.path.isfile(path):
        print("=> loading checkpoint '{}'".format(path))
        checkpoint = torch.load(path, map_location=map_func)
        # print(checkpoint)
        model.load_state_dict(checkpoint['state_dict'], strict=False)
        pretrain(model, checkpoint)
        ckpt_keys = set(checkpoint.keys())
        # print(ckpt_keys)
        own_keys = set(model.state_dict().keys())
        missing_keys = own_keys - ckpt_keys
        for k in missing_keys:
            print('caution: missing keys from checkpoint {}: {}'.format(path, k))

        if optimizer != None:
            best_prec1 = checkpoint['best_prec1']
            last_iter = checkpoint['step']
            optimizer.load_state_dict(checkpoint['optimizer'])
            print("=> also loaded optimizer from checkpoint '{}' (iter {})"
                  .format(path, last_iter))
            return best_prec1, last_iter
    else:
        print("=> no checkpoint found at '{}'".format(path))

def save_checkpoint(state, filename):
    torch.save(state, "{}_iter_{}.pth.tar".format(filename, state['step']))
    

def pretrain(model, state_dict):
    own_state = model.state_dict()
    for name, param in state_dict.items():
        if name in own_state:
            if isinstance(param, torch.nn.Parameter):
                # backwards compatibility for serialized parameters
                param = param.data
            try:
                own_state[name].copy_(param)
            except:
                print('While copying the parameter named {}, '
                      'whose dimensions in the model are {} and '
                      'whose dimensions in the checkpoint are {}.'
                      .format(name, own_state[name].size(), param.size()))
                print("But don't worry about it. Continue pretraining.")

def cal_cer(net_output, target_before, pad_idx=0):
    # import pdb; pdb.set_trace()
    predict = torch.max(net_output, 2)
    predict = predict[1].transpose(1, 0)
    tmp = []
    predict_list = []
    for i in range(predict.size()[0]):
        tmp = list(predict[i].cpu().numpy())
        l2 = tmp[:]
        idx_tmp = idx = 0
        count = 0
        while idx < len(tmp):
            try:
                if tmp[idx] == tmp[idx+count+1]:
                    if (idx+count+2) >= len(tmp):
                        count += 1
                        for _ in range(count):
                            l2.pop(idx_tmp)
                        idx = idx + count + 1
                        count = 0
                        break
                    count +=1
                else:
                    for j in range(count):
                        l2.pop(idx_tmp)
                        # idx_tmp += 1
                    idx_tmp += 1
                    idx = idx+count+1
                    count = 0
                    if (idx + count + 1) >= len(tmp):
                        break
            except:
                import pdb; pdb.set_trace()
                print('len(tmp):', len(tmp))
                print('idx:', idx)
                print('count', count)
                print('predict.size():', predict.size())
                print('tmp:', tmp)
                print('predict:', predict)


        l1 = l2
        # l1.sort(key=tmp.index)
        # l1_tmp = l1
        for j in l1[:]:
            # if j == self.dict.pad() or j == self.dict.unk():
            if j == pad_idx:
                l1.remove(j)
        predict_list.append(l1)
    target_before_list = []
    target_before = target_before
    for i in range(target_before.size()[0]):
        l1 = list(target_before[i].cpu().numpy())
        # tmp = l1
        for j in l1[:]:
            # if j == self.dict.pad() or j == self.dict.unk():
            if j == pad_idx:
                l1.remove(j)
        target_before_list.append(l1)

    # ctc 测试cer
    distance = []
    length_list = []
    word_acc_num = 0
    for i in range(len(target_before_list)):
        ref = target_before_list[i]
        hyp = predict_list[i]
        sm = edit_distance.SequenceMatcher(a=ref, b=hyp)
        edit_dis = sm.distance()
        distance.append(edit_dis)
        length = len(ref)
        length_list.append(length)
        if edit_dis == 0:
            word_acc_num += 1
    return distance, length_list, word_acc_num

def _get_grads(model):
    grads = []
    for name, p in model.named_parameters():
        if not p.requires_grad:
            continue
        if p.grad is None:
            raise RuntimeError('Model parameter did not receive gradient: ' + name + '. '
                               'Use the param in the forward pass or set requires_grad=False')
        grads.append(p.grad.data)
    return grads

def _get_flat_grads(model, out=None):
    grads = _get_grads(model)
    if out is None:
        grads_size = sum(g.numel() for g in grads)
        out = grads[0].new(grads_size).zero_()
    offset = 0
    for g in grads:
        numel = g.numel()
        out[offset:offset+numel].copy_(g.view(-1))
        offset += numel
    return out[:offset]

def _set_flat_grads(model, new_grads):
    grads = _get_grads(model)
    offset = 0
    for g in grads:
        numel = g.numel()
        g.copy_(new_grads[offset:offset+numel].view_as(g))
        offset += numel

def item(tensor):
    if hasattr(tensor, 'item'):
        return tensor.item()
    if hasattr(tensor, '__getitem__'):
        return tensor[0]
    return tensor

def clip_grad_norm_(tensor, max_norm):
    grad_norm = item(torch.norm(tensor))
    if grad_norm == float('inf'):
        grad_norm = float(tensor.abs().max())
    if grad_norm > max_norm > 0:
        clip_coef = max_norm / (grad_norm + 1e-6)
        tensor.mul_(clip_coef)
    return grad_norm

def all_reduce_and_rescale(model, grad_denom, opt, _flat_grads):
    # flatten grads into a single buffer and all-reduce
    flat_grads = _get_flat_grads(model, _flat_grads)
    # if self.args.distributed_world_size > 1:
    #     torch.distributed.all_reduce(flat_grads)

    # rescale and clip gradients
    flat_grads.div_(grad_denom)
    grad_norm = clip_grad_norm_(flat_grads, opt.clip_norm)

    if math.isnan(grad_norm):
        grad_norm = 1
        grads_size = flat_grads.numel()
        flat_grads = flat_grads.new(grads_size).zero_()
    # copy grads back into model parameters
    _set_flat_grads(model, flat_grads)

    return grad_norm, flat_grads

