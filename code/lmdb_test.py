import lmdb
import struct
from pprint import pprint
import cv2
import lz4.block
import numpy as np

root_path = '/data2/mcxu/FormularDatabase/iamLMDB/'
source_path = 'train'
label_list_source_path = '/data2/mcxu/FormularDatabase/iamLMDB/iam.dict'

label_dic = {}

with open(label_list_source_path, 'r') as f:
    for i, character in enumerate(f.readlines(), start=1):   # 这里需要用有序序列嘛？
        label_dic[str(i)] = character.rsplit('\n')[0]

env = lmdb.Environment(root_path + source_path)

txn = env.begin()

# pprint(env.stat())

# size-%09d
id = 728
image_key = 'image-{:09d}'.format(id)
size_key = 'size-{:09d}'.format(id)
label_key = 'label-{:09d}'.format(id)

num_sample = txn.get('num-samples'.encode('utf-8')).decode('utf-8')
print(num_sample)
size_buf = txn.get(size_key.encode('utf-8')).decode('utf-8').split(',')
print(size_buf)
size_buf = [int(i) for i in size_buf]
image_buf = txn.get(image_key.encode('utf-8'))
assert len(size_buf) == 2, 'image size is wrong!'

image_size = size_buf[0] * size_buf[1]
# print(image_size)
image = lz4.block.decompress(image_buf, image_size)
image = struct.unpack(str(image_size)+'c', image)
image = [ord(i) for i in image]
cv_image = np.array(image, dtype=np.int32).reshape(size_buf)
print(cv_image.shape)
cv2.imwrite('test.jpg', cv_image)

label_buf = txn.get(label_key.encode('utf-8')).decode('utf-8')
label_value = label_buf.split(',')
pprint(label_value)

label_str = []

for label in label_value:
    label_str.append(label_dic[label])
print(' '.join(i for i in label_str))
# pprint(label_str)
# for key, value in txn.cursor():
#     print(key, value)

env.close()

if __name__ == '__main__':
    # print(1)
    pass