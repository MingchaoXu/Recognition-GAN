import math
import torch

import torch.nn.functional as F
from torch import nn


class Generator(nn.Module):
    def __init__(self, scale_factor):
        upsample_block_num = int(math.log(scale_factor, 2))

        super(Generator, self).__init__()
        self.block1 = nn.Sequential(
            nn.Conv2d(1, 64, kernel_size=9, padding=4),
            nn.PReLU()
        )
        self.block2 = ResidualBlock(64)
        self.block3 = ResidualBlock(64)
        self.block4 = ResidualBlock(64)
        self.block5 = ResidualBlock(64)
        self.block6 = ResidualBlock(64)
        self.block7 = nn.Sequential(
            nn.Conv2d(64, 64, kernel_size=3, padding=1),
            nn.PReLU()
        )
        block8 = [UpsampleBLock(64, 2) for _ in range(upsample_block_num)]
        block8.append(nn.Conv2d(64, 1, kernel_size=9, padding=4))
        self.block8 = nn.Sequential(*block8)

    def forward(self, x):
        block1 = self.block1(x)
        block2 = self.block2(block1)
        block3 = self.block3(block2)
        block4 = self.block4(block3)
        block5 = self.block5(block4)
        block6 = self.block6(block5)
        block7 = self.block7(block6)
        block8 = self.block8(block1 + block7)
        # return block8
        return (F.tanh(block8) + 1) / 2


class SeqBRNNMM(nn.Module):
    def __init__(self, inputDim, hiddenDim, batchFirst=False, merge=False):
        super(SeqBRNNMM, self).__init__()
        self.Sep2DLSTMModel = nn.Sequential()
        self.inputDim = inputDim
        self.hiddenDim = hiddenDim
        self.batchFirst = batchFirst
        self.merge = merge

        self.forwardModule = nn.LSTM(inputDim, hiddenDim, bidirectional=False)
        self.backwardModule = nn.LSTM(inputDim, hiddenDim, bidirectional=False)

    def forward(self, input):
        # input T,b,c
        b, c, h, w = input.size()
        input1 = input.transpose(2, 3).contiguous().view(b, c, h*w).permute(2, 0, 1)
        input2 = input.flip([3]).transpose(2, 3).contiguous().view(b, c, h*w).permute(2, 0, 1)
        # if self.batchFirst:
        #     input = input.transpose(0, 1)

        forward_out, _ = self.forwardModule(input1)
        # back_input = input.flip([0])
        backward_out, _ = self.backwardModule(input2)
        # backward_out = backward_out.flip([0])
        # import pdb; pdb.set_trace()
        out = torch.cat([forward_out, backward_out], dim=2)
        # if self.batchFirst:
        #     out = out.transpose(0,1)
        return out





class Dis_crnn(nn.Module):

    def __init__(self, imgH, nc, nclass, nh=256, n_rnn=2, realBatch=3):
        super(Dis_crnn, self).__init__()
        # assert imgH % 16 == 0, 'imgH has to be a multiple of 16'
        ks = [3, 3, 3, 3, 3]
        ps = [1, 1, 1, 1, 1]
        ss = [1, 1, 1, 1, 1]
        nm = [16, 32, 48, 64, 80]
        dropProb = [0, 0, 0.2, 0.2, 0.2]

        cnn = nn.Sequential()
        realHei=imgH

        def convRelu(i, batchNormalization=False, dropout=False, leakyRelu=False):
            nIn = nc if i == 0 else nm[i - 1]
            nOut = nm[i]
            if dropout:
                cnn.add_module('dropout{0}'.format(i), nn.Dropout(dropProb[i]))
            cnn.add_module('conv{0}'.format(i),
                           nn.Conv2d(nIn, nOut, ks[i], ss[i], ps[i]))
            if batchNormalization:
                cnn.add_module('batchnorm{0}'.format(i), nn.BatchNorm2d(nOut))
            if leakyRelu:
                cnn.add_module('relu{0}'.format(i),
                               nn.LeakyReLU(0.2, inplace=True))
            else:
                cnn.add_module('relu{0}'.format(i), nn.ReLU(True))


        convRelu(0, batchNormalization=True, dropout=False, leakyRelu=True)
        cnn.add_module('pooling{0}'.format(0), nn.MaxPool2d(2, 2))  # 64x16x64
        convRelu(1, batchNormalization=True, dropout=False, leakyRelu=True)
        cnn.add_module('pooling{0}'.format(0), nn.MaxPool2d(2, 2))
        convRelu(2, batchNormalization=True, dropout=True, leakyRelu=True)
        cnn.add_module('pooling{0}'.format(1), nn.MaxPool2d(2, 2))
        convRelu(3, batchNormalization=True, dropout=True, leakyRelu=True)
        convRelu(4, batchNormalization=True, dropout=True, leakyRelu=True)
        self.cnn = cnn

        self.rnn = nn.Sequential(nn.Dropout(0.5),
                                 SeqBRNNMM(80, 256),
                                 nn.Dropout(0.5),
                                 SeqBRNNMM(256, 256),
                                 nn.Dropout(0.5),
                                 SeqBRNNMM(256, 256),
                                 nn.Dropout(0.5),
                                 SeqBRNNMM(256, 256),
                                 nn.Dropout(0.5),
                                 SeqBRNNMM(256, 256))

        self.linear = nn.Sequential(nn.Dropout(0.5),
                                    nn.Linear(256*2, nclass))

        self.dis = nn.Sequential(nn.AdaptiveAvgPool1d(1),
                                 nn.Conv1d(256, 512, kernel_size=1),
                                 nn.LeakyReLU(0.2),
                                 nn.Conv1d(512, 1, kernel_size=1))


    def forward(self, src_tokens):
        # conv features
        # print(src_tokens.size())
        batch_size = src_tokens.size(0)
        conv = self.cnn(src_tokens)
        # import pdb; pdb.set_trace()
        # rnn_in = torch.sum(conv, dim=2)
        out = self.rnn(conv)
        out = self.linear(out)
        # import pdb; pdb.set_trace()
        # dis_prob = self.dis(rnn_in)
        # rnn_in = rnn_in.transpose(1, 2)

        out = self.rnn(rnn_in) # T,b,c
        return dis_prob, out
        # pred = conv
        # b, c, h, w = conv.size()
        # assert h == 1, "the height of conv must be 1"
        # dis_prob = self.dis(conv)
        # dis_prob = F.sigmoid(dis_prob.view(batch_size))
        # # print(dis_prob.size())
        #
        # conv = conv.squeeze(2)
        #
        # conv = conv.permute(2, 0, 1)  # [w, b, c]
        # # rnn features
        # output = self.rnn(conv)
        # # output = output.transpose(1, 0)
        #
        # return dis_prob, output
        # return dis_prob, output, pred



class ResidualBlock(nn.Module):
    def __init__(self, channels):
        super(ResidualBlock, self).__init__()
        self.conv1 = nn.Conv2d(channels, channels, kernel_size=3, padding=1)
        self.bn1 = nn.BatchNorm2d(channels)
        self.prelu = nn.PReLU()
        self.conv2 = nn.Conv2d(channels, channels, kernel_size=3, padding=1)
        self.bn2 = nn.BatchNorm2d(channels)

    def forward(self, x):
        residual = self.conv1(x)
        residual = self.bn1(residual)
        residual = self.prelu(residual)
        residual = self.conv2(residual)
        residual = self.bn2(residual)

        return x + residual


class UpsampleBLock(nn.Module):
    def __init__(self, in_channels, up_scale):
        super(UpsampleBLock, self).__init__()
        self.conv = nn.Conv2d(in_channels, in_channels * up_scale ** 2, kernel_size=3, padding=1)
        self.pixel_shuffle = nn.PixelShuffle(up_scale)
        self.prelu = nn.PReLU()

    def forward(self, x):
        x = self.conv(x)
        x = self.pixel_shuffle(x)
        x = self.prelu(x)
        return x


if __name__ == "__main__":
    G = Generator(4)
    D = Dis_crnn(imgH=64, nc=1, nclass=36, nh=512, n_rnn=2,)
    D.train()
    for param in D.test.parameters():
        param.requires_grad = False
    # # D2 = Discriminator()
    img = torch.randn(4, 1, 64, 866)
    # # out2 = D2(img)
    dis_prob, out = D(img)
    # print(dis_prob.size(), out.size())
    # out = G(img)
    print(out.size())