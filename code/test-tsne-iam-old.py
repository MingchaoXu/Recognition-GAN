import argparse
import os
import math
import yaml
import cv2
import numpy as np
from math import log10

import pandas as pd
from PIL import Image
import torch.optim as optim
import torch.utils.data
import torchvision
import torchvision.utils as utils
from torchvision.transforms import ToPILImage
from torch.autograd import Variable
from torch.utils.data import DataLoader
from tqdm import tqdm

import pytorch_ssim
from data_utils_iam_v2 import TrainDatasetFromFolder2, ValDatasetFromFolder2, ValDatasetFromFolder_generate,ValDatasetFromFolder_generate2, display_transform, collate_tokens2list
from loss import GeneratorLoss
from warpctc_pytorch import CTCLoss
# from model_reg import Generator, Dis_crnn
from utils_old import *
import torch.nn as nn
import dataset
import importlib

# model
from dbpn import Net as dbpn
from dbpn_v1 import Net as dbpn_v1

parser = argparse.ArgumentParser(description='Train Super Resolution Models')
parser.add_argument('--crop_size', default=88, type=int, help='training images crop size')
parser.add_argument('--upscale_factor', default=4, type=int, choices=[2, 4, 8],
                    help='super resolution upscale factor')
parser.add_argument('--num_epochs', default=100, type=int, help='train epoch number')
parser.add_argument('--device_id', default='[0]', help='train epoch number')
parser.add_argument('--fine_D', default=None, type=str, help='finetune nodel D')
parser.add_argument('--fine_G', default=None, type=str, help='finetune nodel G')
parser.add_argument('--resume-opt', action='store_true')
parser.add_argument('--test', action='store_true')
parser.add_argument('--generate', action='store_true')
parser.add_argument('--config', default='cfg/config1.yaml')

opt = parser.parse_args()

with open(opt.config) as f:
    config = yaml.load(f)
for k, v in config['common'].items():
    setattr(opt, k, v)

print(opt)
CROP_SIZE = opt.crop_size
UPSCALE_FACTOR = opt.upscale_factor
NUM_EPOCHS = opt.num_epochs
# /data2/mcxu/FormularDatabase/ICDAR2015-TextSR-dataset/RELEASE_2015-08-31/DATA/TRAIN/HD
# /data2/mcxu/FormularDatabase/ICDAR2015-TextSR-dataset/RELEASE_2015-08-31/DATA/TEST/HD
# data/VOC2012/train
# data/VOC2012/val
dict_name = opt.dict_name
# import pdb; pdb.set_trace()
str2label = {}
label2str = {}
with open(dict_name, 'r') as f:
    for idx, char in enumerate(f.readlines(), start=1):
        char = char.rsplit('\n')[0]
        str2label[char] = idx

for key in str2label.keys():
    label2str[str(str2label[key])] = key

label2str['0'] = 'blank'

str2label_2 = {}
label2str_2 = {}
with open('/mnt/lustre/xumingchao1/experiment/iamLMDB/iam.dict', 'r') as f:
    for idx, char in enumerate(f.readlines(), start=1):
        char = char.rsplit('\n')[0]
        str2label_2[char] = idx

for key in str2label_2.keys():
    label2str_2[str(str2label_2[key])] = key

# print(len(str2label), len(label2str))

# source_file = '/data2/mcxu/FormularDatabase/coco-icdar/tools/list_train.txt'
# source_file_val = '/data2/mcxu/FormularDatabase/coco-icdar/tools/list_test.txt'
# source_file = '/data2/mcxu/FormularDatabase/90kDICT32px/annotation_train.txt.check.success'
# source_file_val = '/data2/mcxu/FormularDatabase/90kDICT32px/test-10k.txt.check.success'
# source_file_val = '/data2/mcxu/FormularDatabase/IIIT5K/test_list.txt'
# source_file = '/data2/mcxu/FormularDatabase/90kDICT32px/generate_img/hr_list.txt'
# source_file_val = '/data2/mcxu/FormularDatabase/90kDICT32px/generate_img/hr_list.txt'

source_file = opt.source_file
source_file_val = opt.source_file_val
train_root = opt.train_root
val_root = opt.val_root

train_set = TrainDatasetFromFolder2(opt.train_lmdb_path, h_norm=72, crop_size=CROP_SIZE, upscale_factor=UPSCALE_FACTOR)
train_loader = DataLoader(dataset=train_set, collate_fn=train_set.collater, num_workers=4, batch_size=opt.trainbatch, shuffle=False)

if opt.test:
    # val_set = ValDatasetFromFolder2(val_root, source_file=source_file_val, str2label=str2label,
    #                                 upscale_factor=UPSCALE_FACTOR)
    # val_loader = DataLoader(dataset=val_set, collate_fn=val_set.collater, num_workers=4, batch_size=16, shuffle=False)
    val_set = ValDatasetFromFolder_generate(opt.val_lmdb_path, h_norm=72, crop_size=CROP_SIZE, upscale_factor=UPSCALE_FACTOR)
    # val_set = ValDatasetFromFolder_generate2(val_root, h_norm=72, source_file=source_file_val, str2label=str2label, label2str=label2str, str2label_2=str2label_2,upscale_factor=UPSCALE_FACTOR)
    val_loader = DataLoader(dataset=val_set, collate_fn=val_set.collater, num_workers=1, batch_size=opt.testbatch, shuffle=False)
else:
    val_set = ValDatasetFromFolder2(opt.val_lmdb_path, h_norm=72, crop_size=CROP_SIZE, upscale_factor=UPSCALE_FACTOR)
    # val_set = ValDatasetFromFolder2(val_root, h_norm=72, source_file=source_file_val, str2label=str2label, upscale_factor=UPSCALE_FACTOR)
    val_loader = DataLoader(dataset=val_set, collate_fn=val_set.collater, num_workers=4, batch_size=opt.testbatch, shuffle=False)
    
model_module = importlib.import_module(opt.model)

# if not opt.regonly:
netG = model_module.Generator(UPSCALE_FACTOR)
print('# generator parameters:', sum(param.numel() for param in netG.parameters()))

netD = model_module.Dis_crnn_tsne(imgH=72, nc=1, nclass=80, nh=512, n_rnn=2)
print('# discriminator parameters:', sum(param.numel() for param in netD.parameters()))

generator_criterion = GeneratorLoss()
reg_criterion = CTCLoss()

device_ids = eval(opt.device_id)

torch.backends.cudnn.benchmark = True

if torch.cuda.is_available():
    torch.cuda.set_device(device_ids[0])
    # if not opt.regonly:
    netG.cuda()
    netG = nn.DataParallel(netG, device_ids=device_ids)
    netD.cuda()
    generator_criterion.cuda()
    reg_criterion.cuda()
netD = nn.DataParallel(netD, device_ids=device_ids)

# if not opt.regonly:
optimizerG = torch.optim.Adam(netG.parameters(), opt.base_lr)
optimizerG = nn.DataParallel(optimizerG, device_ids=device_ids)
optimizerD = torch.optim.Adam(netD.parameters(), opt.base_lr, betas=(0.9, 0.999), eps=1e-8, weight_decay=1e-4)
optimizerD = nn.DataParallel(optimizerD, device_ids=device_ids)

# print(opt.fine_D)
if opt.fine_D:
    if opt.resume_opt:
        best_prec1, last_iter = load_state(opt.fine_D, netD, optimizer=optimizerD)
    else:
        load_state(opt.fine_D, netD)

if opt.fine_G:
    if opt.resume_opt:
        best_prec1, last_iter = load_state(opt.fine_G, netG, optimizer=optimizerG)
    else:
        load_state(opt.fine_G, netG)
# if opt.fine_D is not None:
#     netD = torch.load(netD, opt.fine_D)

def test(opt, label2str,save_step='current', epoch=None):
    if not opt.regonly:
        netG.eval()
    netD.eval()
    out_path = 'training_results/SRF_ICDAR_' + str(UPSCALE_FACTOR) + '/'
    if not os.path.exists(out_path):
        os.makedirs(out_path)
    val_bar = tqdm(val_loader)
    valing_results = {'mse': 0, 'ssims': 0, 'psnr': 0, 'ssim': 0, 'batch_sizes': 0}
    val_images = []
    sr_distance, sr_length_list, hr_distance, hr_length_list, hr_restore_distance, hr_restore_length_list = [], [], [], [], [], []
    sr_word_acc_num, hr_word_acc_num, hr_restore_word_acc_num = 0, 0, 0
    hr_matrix_list, sr_matrix_list, hr_restore_matrix_list = [], [], []
    for i, data in enumerate(val_bar):
        ori_image = data['ori_image']
        val_lr = data['lr_image']
        val_hr_restore = data['hr_restore_img']
        val_hr = data['hr_image']
        # print(val_lr.size(), val_hr_restore.size(), val_hr.size())
        batch_size = val_lr.size(0)
        valing_results['batch_sizes'] += batch_size
        lr = Variable(val_lr, volatile=True)
        hr = Variable(val_hr, volatile=True)
        hr_restore = Variable(val_hr_restore, volatile=True)
        if torch.cuda.is_available():
            lr = lr.cuda()
            hr = hr.cuda()
            hr_restore = hr_restore.cuda()
        # import pdb; pdb.set_trace()
        # 测试数据正确
        # sample_id, src_tokens, target_tokens, label = data['id'], data['hr_image'], data[
        #     'target_before'], data['target_before']
        
        # # tgt_dict = task.target_dictionary
        # # src_tokens = torch.clamp(src_tokens * 128+127, 0, 255)
        # with open('../image_val/output.txt', 'ab') as f:
        #     for i in range(sample_id.size(0)):
        #         labellist = label[i]
        #         # filepath, labellist = train_set.image_filenames[sample_id[i]]
        #         # cv_image_ori = np.array(cv2.imread(filepath, cv2.IMREAD_GRAYSCALE))
        #         # target_str = val_set.label_dict[str(sample_id[i].item())]
        #         # import pdb; pdb.set_trace()
        #         target_str = [label2str[str(i.item())] for i in labellist]
        #         f.write(
        #             'T-{}\t{}\t{}\t{}\n'.format(sample_id[i], target_str, src_tokens[i].size(), target_tokens[i],
        #                                         ).encode('gb18030'))
        #         # ToPILImage()(src_tokens[i]).save('../image_train/train_{}.jpg'.format(sample_id[i]))
        #         cv_image = np.array(src_tokens[i] * 256, dtype=np.int32).transpose(1, 2, 0)
        #         cv2.imwrite('../image_val/val_{}.jpg'.format(sample_id[i]), cv_image)
        # if not opt.regonly:
        sr = netG(lr)
        sr_prob_out, sr_reg_out, sr_pred = netD(sr)
        # model v3 需要transpose一下，因为并行的时候最后整合时，实在0维concat，所以网络输出应该是b，*，*
        sr_reg_out = sr_reg_out.transpose(0, 1)

        sr_distance_tmp, sr_length_list_tmp, sr_word_acc_num_tmp, _ = cal_cer_iam(sr_reg_out, data['target_before'],
                                                                            pad_idx=0)
        sr_distance.extend(sr_distance_tmp)
        sr_length_list.extend(sr_length_list_tmp)
        sr_word_acc_num += sr_word_acc_num_tmp
        # else:
        #     sr = hr
        #     sr_distance = [0]
        hr_prob_out, hr_reg_out, hr_pred = netD(hr)
        # model v3 需要transpose一下，因为并行的时候最后整合时，实在0维concat，所以网络输出应该是b，*，*
        hr_reg_out = hr_reg_out.transpose(0, 1)

        hr_restore_prob_out, hr_restore_reg_out, hr_restore_pred = netD(hr_restore)
        # model v3 需要transpose一下，因为并行的时候最后整合时，实在0维concat，所以网络输出应该是b，*，*
        hr_restore_reg_out = hr_restore_reg_out.transpose(0, 1)

        # tsne 特征存储
        hr_matrix_list.append(hr_pred.data.cpu())
        sr_matrix_list.append(sr_pred.data.cpu())
        hr_restore_matrix_list.append(hr_restore_pred.data.cpu())

        sr_distance_tmp, sr_length_list_tmp, sr_word_acc_num_tmp, _ = cal_cer_iam(sr_reg_out, data['target_before'], pad_idx=0)
        hr_distance_tmp, hr_length_list_tmp, hr_word_acc_num_tmp, hr_word_all_tmp = cal_cer_iam(hr_reg_out, data['target_before'], pad_idx=0)
        hr_restore_distance_tmp, hr_restore_length_list_tmp, hr_restore_word_acc_num_tmp, _ = cal_cer_iam(hr_restore_reg_out, data['target_before'],
                                                                      pad_idx=0)
        sr_distance.extend(sr_distance_tmp)
        sr_length_list.extend(sr_length_list_tmp)
        hr_distance.extend(hr_distance_tmp)
        hr_length_list.extend(hr_length_list_tmp)
        hr_restore_distance.extend(hr_restore_distance_tmp)
        hr_restore_length_list.extend(hr_restore_length_list_tmp)
        # word acc num
        sr_word_acc_num += sr_word_acc_num_tmp
        hr_word_acc_num += hr_word_acc_num_tmp
        hr_restore_word_acc_num += hr_restore_word_acc_num_tmp

        batch_mse = ((sr - hr) ** 2).data.mean()
        valing_results['mse'] += batch_mse * batch_size
        batch_ssim = pytorch_ssim.ssim(sr, hr).data[0]
        valing_results['ssims'] += batch_ssim * batch_size
        valing_results['psnr'] = 10 * log10(1 / (valing_results['mse'] / valing_results['batch_sizes']))
        valing_results['ssim'] = valing_results['ssims'] / valing_results['batch_sizes']
        val_bar.set_description(
            desc='[converting LR images to SR images] PSNR: %.4f dB SSIM: %.4f' % (
                valing_results['psnr'], valing_results['ssim']))
        # print(val_hr_restore.size())
        if opt.generate:
            for i in range(val_hr.size(0)):
                # print(ori_image[i])
                ori_img = ToPILImage()(ori_image[i])
                hr_img = ToPILImage()(val_hr[i])
                hr_restore_img = ToPILImage()(val_hr_restore[i])
                # import pdb
                # pdb.set_trace()
                lr_img = ToPILImage()(val_lr[i])
                sr_img = ToPILImage()(sr[i].detach().cpu())

                hr_img_name, label_list = val_set.image_filenames[data['id'][i]]
                target_str = [label2str[str(x)] for x in label_list]
                target_str = ''.join(target_str)
                _, hr_img_name = os.path.split(hr_img_name)
                # print(hr_img_name)
                hr_img_name = os.path.join(opt.generate_img, hr_img_name)
                # print(opt.generate_img, hr_img_name)
                img_floder, name = os.path.split(hr_img_name)
                name = os.path.splitext(name)[0]
                # print(img_floder, name)
                if not os.path.isdir(img_floder):
                    os.makedirs(img_floder)
                ori_img_name = os.path.join(img_floder, name+'_ori.png')
                # print(ori_img_name)
                ori_img.save(ori_img_name)
                hr_img_name = os.path.join(img_floder, name+'_hr.png')
                hr_img.save(hr_img_name)
                hr_restore_img_name = os.path.join(img_floder, name+'_hr_restore.png')
                hr_restore_img.save(hr_restore_img_name)
                lr_img_name = os.path.join(img_floder, name+'_lr.png')
                lr_img.save(lr_img_name)
                sr_img_name = os.path.join(img_floder, name+'_sr.png')
                sr_img.save(sr_img_name)
                gt_name = os.path.join(img_floder, name+'_gt.txt')
                with open(gt_name, 'w') as f:
                    f.write(target_str+'\n')
            # print(lr_img_name)
            # break




        if i % 100 == 0:
            # import pdb; pdb.set_trace()
            val_images.extend(
                [display_transform(val_hr_restore.size(3), val_hr_restore.size(2))(val_hr_restore[0]),
                display_transform(hr.data.cpu().size(3), hr.data.cpu().size(2))(hr.data.cpu()[0]),
                display_transform(sr.data.cpu().size(3), sr.data.cpu().size(2))(sr.data.cpu()[0])])
    
    sr_save = torch.cat(sr_matrix_list, 0)
    hr_save = torch.cat(hr_matrix_list, 0)
    hr_restore_save = torch.cat(hr_restore_matrix_list, 0)

    if not os.path.exists(opt.tsne_path):
        os.makedirs(opt.tsne_path)
    # print(os.path.join(opt.tsne_path, '{}_sr.npy'.format(opt.fine_D.split('/')[-1])))

    np.save(os.path.join(opt.tsne_path, '{}_sr-black.npy'.format(opt.fine_D.split('/')[-1])), np.array(sr_save))
    np.save(os.path.join(opt.tsne_path, '{}_hr-black.npy'.format(opt.fine_D.split('/')[-1])), np.array(hr_save))
    np.save(os.path.join(opt.tsne_path, '{}_hr_restore-black.npy'.format(opt.fine_D.split('/')[-1])), np.array(hr_restore_save))

    sr_cer = sum(sr_distance) / sum(sr_length_list)
    hr_cer = sum(hr_distance) / sum(hr_length_list)
    hr_restore_cer = sum(hr_restore_distance) / sum(hr_restore_length_list)

    sr_word_acc = sr_word_acc_num / len(val_set)
    hr_word_acc = hr_word_acc_num / len(val_set)
    hr_restore_word_acc = hr_restore_word_acc_num / len(val_set)

    print('\n')
    print('sr_cer: %.4f hr_cer: %.4f hr_restore_cer: %.4f' % (sr_cer, hr_cer, hr_restore_cer))
    print('sr_acc: %.4f hr_acc: %.4f hr_restore_acc: %.4f' % (sr_word_acc, hr_word_acc, hr_restore_word_acc))
    # torchvision.transforms.ToPILImage()(val_images[0]).save('img.png')
    # print(val_images.size(0))
    # for img in val_images:
    #     print(img.size() )
    # val_images = collate_tokens2list(val_images, pad_value=0)
    # torchvision.transforms.ToPILImage()(val_images[0]).save('img2.png')
    # for img in val_images:
    #     print(img.size() )
    # print(val_images[0])
    # val_images = torch.stack(val_images)
    # print(val_images.size(0), math.ceil(val_images.size(0) / 15))
    # val_images = torch.chunk(val_images, math.ceil(val_images.size(0) / 15))
    # print(len(val_images))
    # val_save_bar = tqdm(val_images, desc='[saving training results]')
    # index = 1
    # for image in val_save_bar:
    #     print(len(image))
        # image = utils.make_grid(image, nrow=3, padding=5)
        # utils.save_image(image, out_path + 'test_index_%d.png' % (index,), padding=5)
        # index += 1
    if not opt.test:
        print('current step: {}, epoch: {}'.format(save_step, epoch))
        if not os.path.exists(opt.model_path):
            os.makedirs(opt.model_path)
        torch.save(netG.state_dict(), os.path.join(opt.model_path, 'netG_epoch_%d_%d_iter_%s.pth' % (UPSCALE_FACTOR, epoch, save_step)))
        torch.save(netD.state_dict(), os.path.join(opt.model_path, 'netD_epoch_%d_%d_iter_%s.pth' % (UPSCALE_FACTOR, epoch, save_step)))

    netG.train()
    netD.train()


    
if opt.test:
    test(opt, label2str)
    exit()

results = {'d_loss': [], 'g_loss': [], 'd_score': [], 'g_score': [], 'psnr': [], 'ssim': [],  'sr_cer': [], 'hr_cer': [], 'hr_restore_cer': []}
flat_grads = None
for epoch in range(1, NUM_EPOCHS + 1):
    train_bar = tqdm(train_loader)
    running_results = {'batch_sizes': 0, 'd_loss': 0, 'g_loss': 0, 'reg_loss': 0,'d_score': 0, 'g_score': 0}

    netG.train()
    netD.train()
    if opt.sronly:
        for param in netD.rnn.parameters():
            param.requires_grad = False
    if opt.regonly:
        for param in netG.parameters():
            param.requires_grad = False
        for param in netD.dis.parameters():
            param.requires_grad = False
    distance, length_list = [], []
    for i, data in enumerate(train_bar):
        g_update_first = True
        lr_image = data['lr_image']
        batch_size = lr_image.size(0)
        running_results['batch_sizes'] += batch_size

        ############################
        # (1) Update D network: maximize D(x)-1-D(G(z))
        ###########################
        real_img = Variable(data['hr_image'])
        
        # print(real_img.size())
        if torch.cuda.is_available():
            real_img = real_img.cuda()
        # 测试数据正确
        # sample_id, src_tokens, target_tokens = data['id'], data['hr_image'], data[
        #     'target_before']
        
        # # tgt_dict = task.target_dictionary
        # src_tokens = torch.clamp(src_tokens * 255, 0, 255)
        # with open('image_train/output.txt', 'ab') as f:
        #     for i in range(sample_id.size(0)):
        #         filepath, labellist = train_set.image_filenames[sample_id[i]]
        #         cv_image_ori = np.array(cv2.imread(filepath, cv2.IMREAD_GRAYSCALE))
        #         target_str = [label2str[str(i)] for i in labellist]
        #         f.write(
        #             'T-{}\t{}\t{}\t{}\t{}\n'.format(sample_id[i], target_str, src_tokens[i].size(), target_tokens[i],
        #                                             filepath).encode('gb18030'))
        #         cv_image = np.array(src_tokens[i], dtype=np.int32).transpose(1, 2, 0)
        #         cv2.imwrite('image_train/train_{}.jpg'.format(sample_id[i]), cv_image)
        #         # cv2.imwrite('image_train/train_{}_ori.jpg'.format(sample_id[i]), cv_image_ori)
        z = Variable(lr_image)
        if torch.cuda.is_available():
            z = z.cuda()
        fake_img = netG(z)

        netD.zero_grad()
        real_prob_out, real_reg_out = netD(real_img)
        real_prob_out = real_prob_out.mean()
        fake_prob_out, fake_reg_out = netD(fake_img)
        fake_prob_out = fake_prob_out.mean()
        d_loss = 1 - real_prob_out + fake_prob_out

        target = data['target'].int().cpu()
        preds_size = torch.IntTensor([real_reg_out.size(0)] * real_reg_out.size(1))
        length = torch.IntTensor(data['ntokens'])
        reg_loss = reg_criterion(real_reg_out, target, preds_size, length).cuda()
        if not opt.sronly:
            reg_loss_fake = reg_criterion(fake_reg_out, target, preds_size, length).cuda()
            d_loss = d_loss + opt.reg_w*reg_loss + opt.reg_fake_w*reg_loss_fake
            if torch.isnan(reg_loss):
                optimizerD.zero_grad()
                continue
        elif opt.regonly:
            d_loss = reg_loss
            if torch.isnan(reg_loss):
                optimizerD.zero_grad()
                continue
        else:
            d_loss = d_loss
        d_loss.backward(retain_graph=True)
        
        grad_norm, flat_grads = all_reduce_and_rescale(netD, batch_size, opt, flat_grads)
        optimizerD.step()

        distance_tmp, length_list_tmp, word_acc_num_tmp, _ = cal_cer_iam(real_reg_out, data['target_before'], pad_idx=0)
        distance.extend(distance_tmp)
        length_list.extend(length_list_tmp)
        cer = sum(distance) / sum(length_list)
        ############################
        # (2) Update G network: minimize 1-D(G(z)) + Perception Loss + Image Loss + TV Loss
        ###########################
        if not opt.regonly:
            netG.zero_grad()
            g_loss = generator_criterion(fake_prob_out, fake_img, real_img)
            g_loss.backward()
            optimizerG.step()
            fake_img = netG(z)
            fake_prob_out, fake_reg_out = netD(fake_img)
            fake_prob_out = fake_prob_out.mean()

            g_loss = generator_criterion(fake_prob_out, fake_img, real_img)
            running_results['g_loss'] += g_loss.data[0] * batch_size
            d_loss = 1 - real_prob_out + fake_prob_out
            running_results['cer'] = cer
            running_results['d_loss'] += d_loss.data[0] * batch_size
            running_results['reg_loss'] += reg_loss.data[0]
            running_results['d_score'] += real_prob_out.data[0] * batch_size
            running_results['g_score'] += fake_prob_out.data[0] * batch_size

            train_bar.set_description(desc='[%d/%d] Loss_D: %.4f Loss_G: %.4f D(x): %.4f D(G(z)): %.4f CER: %.4f' % (
                epoch, NUM_EPOCHS, running_results['d_loss'] / running_results['batch_sizes'],
                running_results['g_loss'] / running_results['batch_sizes'],
                running_results['d_score'] / running_results['batch_sizes'],
                running_results['g_score'] / running_results['batch_sizes'], 
                running_results['cer']))
        else:
            running_results['cer'] = cer
            running_results['reg_loss'] += reg_loss.data[0]

            train_bar.set_description(desc='[%d/%d] reg_loss: %.4f CER: %.4f' % (
                epoch, NUM_EPOCHS, running_results['reg_loss'] / running_results['batch_sizes'],
                running_results['cer']))

        if i % opt.val_freq == 0:
            # print(i)
            test(opt, label2str, str(i),epoch=epoch)

    netG.eval()
    netD.eval()
    out_path = opt.save_path
    # out_path = 'training_results/SRF_ICDAR_800w_' + str(UPSCALE_FACTOR) + '/'
    if not os.path.exists(out_path):
        os.makedirs(out_path)
    val_bar = tqdm(val_loader)
    valing_results = {'mse': 0, 'ssims': 0, 'psnr': 0, 'ssim': 0, 'batch_sizes': 0}
    val_images = []
    sr_distance, sr_length_list, hr_distance, hr_length_list, hr_restore_distance, hr_restore_length_list = [], [], [], [], [], []
    sr_word_acc_num, hr_word_acc_num, hr_restore_word_acc_num = 0, 0, 0
    for i, data in enumerate(val_bar):
        val_lr = data['lr_image']
        val_hr_restore = data['hr_restore_img']
        val_hr = data['hr_image']
        # print(val_lr.size(), val_hr_restore.size(), val_hr.size())
        batch_size = val_lr.size(0)
        valing_results['batch_sizes'] += batch_size
        lr = Variable(val_lr, volatile=True)
        hr = Variable(val_hr, volatile=True)
        hr_restore = Variable(val_hr_restore, volatile=True)
        if torch.cuda.is_available():
            lr = lr.cuda()
            hr = hr.cuda()
            hr_restore = hr_restore.cuda()
        sr = netG(lr)
        sr_prob_out, sr_reg_out = netD(sr)
        hr_prob_out, hr_reg_out = netD(hr)
        hr_restore_prob_out, hr_restore_reg_out = netD(hr_restore)
        sr_distance_tmp, sr_length_list_tmp, sr_word_acc_num_tmp, _ = cal_cer_iam(sr_reg_out, data['target_before'], pad_idx=0)
        hr_distance_tmp, hr_length_list_tmp, hr_word_acc_num_tmp, _ = cal_cer_iam(hr_reg_out, data['target_before'], pad_idx=0)
        hr_restore_distance_tmp, hr_restore_length_list_tmp, hr_restore_word_acc_num_tmp, _ = cal_cer_iam(hr_restore_reg_out, data['target_before'],
                                                                      pad_idx=0)
        sr_distance.extend(sr_distance_tmp)
        sr_length_list.extend(sr_length_list_tmp)
        hr_distance.extend(hr_distance_tmp)
        hr_length_list.extend(hr_length_list_tmp)
        hr_restore_distance.extend(hr_restore_distance_tmp)
        hr_restore_length_list.extend(hr_restore_length_list_tmp)
        # word acc num
        sr_word_acc_num += sr_word_acc_num_tmp
        hr_word_acc_num += hr_word_acc_num_tmp
        hr_restore_word_acc_num += hr_restore_word_acc_num_tmp

        batch_mse = ((sr - hr) ** 2).data.mean()
        valing_results['mse'] += batch_mse * batch_size
        batch_ssim = pytorch_ssim.ssim(sr, hr).data[0]
        valing_results['ssims'] += batch_ssim * batch_size
        valing_results['psnr'] = 10 * log10(1 / (valing_results['mse'] / valing_results['batch_sizes']))
        valing_results['ssim'] = valing_results['ssims'] / valing_results['batch_sizes']
        val_bar.set_description(
            desc='[converting LR images to SR images] PSNR: %.4f dB SSIM: %.4f' % (
                valing_results['psnr'], valing_results['ssim']))
        # print(val_hr_restore.size())
        if i % 100 == 0:
            val_images.extend(
                [display_transform(val_hr_restore.size(3), val_hr_restore.size(2))(val_hr_restore[0].squeeze(0)),
                display_transform(hr.data.cpu().size(3), hr.data.cpu().size(2))(hr.data.cpu()[0].squeeze(0)),
                display_transform(sr.data.cpu().size(3), sr.data.cpu().size(2))(sr.data.cpu()[0].squeeze(0))])
    
    sr_cer = sum(sr_distance) / sum(sr_length_list)
    hr_cer = sum(hr_distance) / sum(hr_length_list)
    hr_restore_cer = sum(hr_restore_distance) / sum(hr_restore_length_list)

    # word acc
    sr_word_acc = sr_word_acc_num / len(val_set)
    hr_word_acc = hr_word_acc_num / len(val_set)
    hr_restore_word_acc = hr_restore_word_acc_num / len(val_set)

    print('sr_cer: %.4f hr_cer: %.4f hr_restore_cer: %.4f' % (sr_cer, hr_cer, hr_restore_cer))
    print('sr_acc: %.4f hr_acc: %.4f hr_restore_acc: %.4f' % (sr_word_acc, hr_word_acc, hr_restore_word_acc))
    # torchvision.transforms.ToPILImage()(val_images[0]).save('img.png')
    # print(val_images.size(0))
    # for img in val_images:
    #     print(img.size() )
    val_images = collate_tokens2list(val_images, pad_value=0)
    # torchvision.transforms.ToPILImage()(val_images[0]).save('img2.png')
    # for img in val_images:
    #     print(img.size() )
    # print(val_images[0])
    val_images = torch.stack(val_images)
    # print(val_images.size(0), math.ceil(val_images.size(0) / 15))
    val_images = torch.chunk(val_images, math.ceil(val_images.size(0) / 15))
    # print(len(val_images))
    val_save_bar = tqdm(val_images, desc='[saving training results]')
    index = 1
    for image in val_save_bar:
        # print(len(image))
        image = utils.make_grid(image, nrow=3, padding=5)
        utils.save_image(image, out_path + 'epoch_%d_index_%d.png' % (epoch, index), padding=5)
        index += 1

    # save model parameters
    if not os.path.exists(opt.model_path):
        os.makedirs(opt.model_path)
    torch.save(netG.state_dict(), os.path.join(opt.model_path, 'netG_epoch_%d_%d.pth' % (UPSCALE_FACTOR, epoch)))
    torch.save(netD.state_dict(), os.path.join(opt.model_path, 'netD_epoch_%d_%d.pth' % (UPSCALE_FACTOR, epoch)))
    # save loss\scores\psnr\ssim
    results['d_loss'].append(running_results['d_loss'] / running_results['batch_sizes'])
    results['g_loss'].append(running_results['g_loss'] / running_results['batch_sizes'])
    results['d_score'].append(running_results['d_score'] / running_results['batch_sizes'])
    results['g_score'].append(running_results['g_score'] / running_results['batch_sizes'])
    results['psnr'].append(valing_results['psnr'])
    results['ssim'].append(valing_results['ssim'])
    results['sr_cer'].append(sr_cer)
    results['hr_cer'].append(hr_cer)
    results['hr_restore_cer'].append(hr_restore_cer)

    if epoch % 10 == 0 and epoch != 0:
        out_path = 'statistics/'
        data_frame = pd.DataFrame(
            data={'Loss_D': results['d_loss'], 'Loss_G': results['g_loss'], 'Score_D': results['d_score'],
                  'Score_G': results['g_score'], 'PSNR': results['psnr'], 'SSIM': results['ssim'],
                  'sr_cer': results['sr_cer'],'hr_cer': results['hr_cer'], 'hr_restore_cer': results['hr_restore_cer']},
            index=range(1, epoch + 1))
        data_frame.to_csv(out_path + 'srf_' + str(UPSCALE_FACTOR) + '_train_results.csv', index_label='Epoch')



