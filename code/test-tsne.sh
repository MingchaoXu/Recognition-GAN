python test-tsne.py --device_id 1 \
 --crop_size 32 \
 --config cfg/config-v2-1,0.1.yaml \
 --fine_G ../epochs/v2-1,0.1/netG_epoch_4_1_iter_180000.pth \
  --fine_D ../finetune_model/crnn_recognition.pth \
 --test \
#  --fine_G ../epochs/v2-1,0.1/netG_epoch_4_1_iter_180000.pth \
  #-fine_D ../epochs/v2-1,0.1/netD_epoch_4_1_iter_180000.pth \
# --generate \
#  --fine_D ../finetune-model/checkpoint130.pt
