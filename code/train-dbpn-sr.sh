python -u train_dbpn.py --device_id 2 \
 --crop_size 32 \
 --config cfg/config-dbpn.yaml \
 --test \
--fine_G ../epochs/37class-dbpn/netG_epoch_4_2_iter_25000.pth \
 --fine_D ../finetune_model/crnn_recognition.pth \
# --generate \
#  --fine_D ../finetune-model/checkpoint130.pt
