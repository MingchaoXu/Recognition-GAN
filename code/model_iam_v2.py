import math
import torch

import torch.nn.functional as F
from torch import nn


class Generator(nn.Module):
    def __init__(self, scale_factor):
        upsample_block_num = int(math.log(scale_factor, 2))

        super(Generator, self).__init__()
        self.block1 = nn.Sequential(
            nn.Conv2d(1, 64, kernel_size=9, padding=4),
            nn.PReLU()
        )
        self.block2 = ResidualBlock(64)
        self.block3 = ResidualBlock(64)
        self.block4 = ResidualBlock(64)
        self.block5 = ResidualBlock(64)
        self.block6 = ResidualBlock(64)
        self.block7 = nn.Sequential(
            nn.Conv2d(64, 64, kernel_size=3, padding=1),
            nn.PReLU()
        )
        block8 = [UpsampleBLock(64, 2) for _ in range(upsample_block_num)]
        block8.append(nn.Conv2d(64, 1, kernel_size=9, padding=4))
        self.block8 = nn.Sequential(*block8)

    def forward(self, x):
        block1 = self.block1(x)
        block2 = self.block2(block1)
        block3 = self.block3(block2)
        block4 = self.block4(block3)
        block5 = self.block5(block4)
        block6 = self.block6(block5)
        block7 = self.block7(block6)
        block8 = self.block8(block1 + block7)
        # return block8
        return (F.tanh(block8) + 1) / 2


class MDLSTM(nn.Module):

    def __init__(self, nIn, nOut):
        super(MDLSTM, self).__init__()

        self.hrnn1 = nn.LSTM(nIn, nOut, bidirectional=True)
        self.hrnn2 = nn.LSTM(nIn, nOut, bidirectional=True)

    def forward(self, input):
        b, c, h, w = input.size()
        input1 = input.transpose(2,3)
        input1 = input1.contiguous().view(b, c, h * w).permute(2, 0, 1)
        input2 = input1.flip([0])
        recurrent1, _ = self.hrnn1(input1)
        recurrent2, _ = self.hrnn2(input2)
        recurrent1 = recurrent1.view(recurrent1.size(0), recurrent1.size(1), 2, recurrent1.size(2)//2)
        recurrent2 = recurrent2.view(recurrent2.size(0), recurrent2.size(1), 2, recurrent2.size(2)//2)
        recurrent1_1 = recurrent1[:, :, 0, :]
        recurrent1_2 = recurrent1[:, :, 1, :]
        recurrent2_1 = recurrent2[:, :, 0, :]
        recurrent2_2 = recurrent2[:, :, 1, :]
        output = (recurrent1_1 + recurrent1_2 + recurrent2_1 + recurrent2_2)/4
        output = output.permute(1, 2, 0).view(b, -1, h, w)
        # T, b, h = recurrent.size()
        # t_rec = recurrent.view(T * b, h)
        # recurrent, _ = self.vrnn(recurrent)
        # output = self.embedding(t_rec)  # [T * b, nOut]
        # output = output.view(T, b, -1)

        return output


class Dis_crnn(nn.Module):

    def __init__(self, imgH, nc, nclass, nh=256, n_rnn=2, leakyRelu=False):
        super(Dis_crnn, self).__init__()
        assert imgH % 16 == 0, 'imgH has to be a multiple of 16'

        self.C_1 = nn.Sequential(
            nn.Conv2d(1, 25, kernel_size=3, stride=1, padding=1),
            nn.MaxPool2d((2, 2)),
            nn.Tanh()
        )
        self.LP_1 = nn.Sequential(
            nn.Conv2d(25, 50, kernel_size=3, stride=1, padding=1),
            nn.Dropout2d(0.25),
            nn.MaxPool2d((2, 2)),
            nn.Tanh(),
            MDLSTM(50, 75),
            nn.Dropout2d(0.25),
        )
        self.LP_2 = nn.Sequential(
            nn.Conv2d(75, 100, kernel_size=3, stride=1, padding=1),
            nn.Dropout2d(0.25),
            nn.MaxPool2d((2, 1)),
            nn.Tanh(),
            MDLSTM(100, 125),
            nn.Dropout2d(0.25),
        )
        self.LP_3 = nn.Sequential(
            nn.Conv2d(125, 150, kernel_size=3, stride=1, padding=1),
            nn.Dropout2d(0.25),
            nn.MaxPool2d((2, 1)),
            nn.Tanh(),
            MDLSTM(150, 175),
            nn.Dropout2d(0.25),
        )
        self.rnn = nn.Sequential(
            MDLSTM(175, nclass),
            nn.Dropout2d(0.25),
        )


        # self.rnn = nn.Sequential(
        #     BidirectionalLSTM(512, nh, nh),
        #     BidirectionalLSTM(nh, nh, nclass))
        #
        self.dis = nn.Sequential(nn.AdaptiveAvgPool2d(1),
                                 nn.Conv2d(175, 512, kernel_size=1),
                                 nn.LeakyReLU(0.2),
                                 nn.Conv2d(512, 1, kernel_size=1))
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.xavier_uniform_(m.weight, gain=1)
            elif isinstance(m, nn.LSTM):
                nn.init.xavier_uniform_(m.all_weights[0][0], gain=1)
                nn.init.xavier_uniform_(m.all_weights[0][1], gain=1)
                nn.init.xavier_uniform_(m.all_weights[1][0], gain=1)
                nn.init.xavier_uniform_(m.all_weights[1][1], gain=1)
    def forward(self, src_tokens):
        # conv features
        # print(src_tokens.size())
        batch_size = src_tokens.size(0)
        out = self.C_1(src_tokens)
        out = self.LP_1(out)
        out = self.LP_2(out)
        out = self.LP_3(out)
        dis_out = out
        dis_prob = self.dis(dis_out)
        out = self.rnn(out)
        # import pdb; pdb.set_trace()
        out = torch.sum(out, 2)
        out = out.view(out.size(2), out.size(0), -1)
        out = F.softmax(out, dim=2)
        # return out
        return dis_prob, out
        # return dis_prob, output, pred

class ResidualBlock(nn.Module):
    def __init__(self, channels):
        super(ResidualBlock, self).__init__()
        self.conv1 = nn.Conv2d(channels, channels, kernel_size=3, padding=1)
        self.bn1 = nn.BatchNorm2d(channels)
        self.prelu = nn.PReLU()
        self.conv2 = nn.Conv2d(channels, channels, kernel_size=3, padding=1)
        self.bn2 = nn.BatchNorm2d(channels)

    def forward(self, x):
        residual = self.conv1(x)
        residual = self.bn1(residual)
        residual = self.prelu(residual)
        residual = self.conv2(residual)
        residual = self.bn2(residual)

        return x + residual


class UpsampleBLock(nn.Module):
    def __init__(self, in_channels, up_scale):
        super(UpsampleBLock, self).__init__()
        self.conv = nn.Conv2d(in_channels, in_channels * up_scale ** 2, kernel_size=3, padding=1)
        self.pixel_shuffle = nn.PixelShuffle(up_scale)
        self.prelu = nn.PReLU()

    def forward(self, x):
        x = self.conv(x)
        x = self.pixel_shuffle(x)
        x = self.prelu(x)
        return x


if __name__ == "__main__":
    from PIL import Image
    from torchvision.transforms import ToTensor
    a = torch.tensor([0, 1, 2, 3]).view(2,2)
    b = a.flip([1])
    G = Generator(4)
    D = Dis_crnn(imgH=64, nc=1, nclass=36, nh=512, n_rnn=2, leakyRelu=False)
    # # D2 = Discriminator()
    path = '/data2/mcxu/FormularDatabase/iam/data/b04/b04-010/b04-010-02.png'
    # img = ToTensor()(Image.open(path).convert('L'))
    img = torch.randn(1, 1, 128, 1666)
    # out2 = D2(img)
    out = D(img)
    # print(dis_prob.size(), out.size())
    # out = G(img)
    print(out.size())