set -o errexit

. ../scripts/common.sh

# check arguments
if [ $# != 4 ]; then
    info_red "Usage: train <partition> <JOB_NAME> <DEVICE_ID> <CONFIG>"
    exit 55
fi

JOB_NAME=$2
DEVICE_ID=$3
CONFIG=$4

START_TIME=`date +%m%d-%H%M%S`
mkdir -p logs
LOG_FILE=logs/train-$JOB_NAME-$START_TIME.log

info_green "Now start to submit task."
GLOG_vmodule=MemcachedClient=-1 \
    srun --mpi=pmi2 -p $1 --gres=gpu:1 \
    --job-name=$JOB_NAME \
    python -u train_scene-v2.py --device_id $DEVICE_ID \
    --crop_size 32 \
    --config $CONFIG \
    --test
    # 2>&1 | tee $LOG_FILE > /dev/null &
    # --resume-opt \

# --test \
info_green "Task \"$JOB_NAME\" submitted."
info_green "Please check log: \"$LOG_FILE\" for details."
# --fine_G ../epochs/v2-1,0.1/netG_epoch_4_1_iter_180000.pth \
#  --fine_D ../epochs/v2-1,0.1/netD_epoch_4_1_iter_180000.pth \
#--test \
# --generate \
#  --fine_D ../finetune_model/crnn_recognition.pth \
# --fine_G ../epochs/37class/netG_epoch_4_1.pth \
