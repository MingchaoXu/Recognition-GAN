import itertools
import torch
import numpy as np
import contextlib

class CountingIterator(object):
    """Wrapper around an iterable that maintains the iteration count."""

    def __init__(self, iterable):
        self.iterable = iterable
        self.count = 0
        self.itr = iter(self)

    def __len__(self):
        return len(self.iterable)

    def __iter__(self):
        for x in self.iterable:
            self.count += 1
            yield x

    def __next__(self):
        return next(self.itr)

    def has_next(self):
        return self.count < len(self)

    def skip(self, num_to_skip):
        next(itertools.islice(self.itr, num_to_skip, num_to_skip), None)
        return self

class ShardedIterator(object):
    """A sharded wrapper around an iterable (padded to length)."""

    def __init__(self, iterable, num_shards, shard_id, fill_value=None):
        if shard_id < 0 or shard_id >= num_shards:
            raise ValueError('shard_id must be between 0 and num_shards')

        self._sharded_len = len(iterable) // num_shards
        if len(iterable) % num_shards > 0:
            self._sharded_len += 1

        self.itr = itertools.zip_longest(
            range(self._sharded_len),
            itertools.islice(iterable, shard_id, len(iterable), num_shards),
            fillvalue=fill_value,
        )

    def __len__(self):
        return self._sharded_len

    def __iter__(self):
        return self

    def __next__(self):
        return next(self.itr)[1]


class EpochBatchIterator(object):
    def __init__(
        self, dataset, max_tokens=None, max_sentences=None, max_positions=None,
        ignore_invalid_inputs=True, required_batch_size_multiple=1, seed=1,
        num_shards=1, shard_id=0,
    ):
        self.dataset = dataset
        self.max_tokens = max_tokens if max_tokens is not None else float('Inf')
        self.max_sentences = max_sentences if max_sentences is not None else float('Inf')
        self.max_positions = max_positions
        self.ignore_invalid_inputs = ignore_invalid_inputs
        self.bsz_mult = required_batch_size_multiple
        self.seed = seed
        self.num_shards = num_shards
        self.shard_id = shard_id

        with numpy_seed(self.seed):
            self.frozen_batches = tuple(self._batch_generator())

        self.epoch = 0
        self._cur_epoch_itr = None
        self._next_epoch_itr = None


    def next_epoch_itr(self, shuffle=True):
        """Shuffle batches and return a new iterator over the dataset."""
        if self._next_epoch_itr is not None:
            self._cur_epoch_itr = self._next_epoch_itr
            self._next_epoch_itr = None
        else:
            self.epoch += 1
            if self.epoch == 1:
                print('First epoch no shuffle')
                self._cur_epoch_itr = self._get_iterator_for_epoch(self.epoch, shuffle=False)
            else:
                self._cur_epoch_itr = self._get_iterator_for_epoch(self.epoch, shuffle=True)
        return self._cur_epoch_itr

    def _get_iterator_for_epoch(self, epoch, shuffle):
        if shuffle:
            # set seed based on the seed and epoch number so that we get
            # reproducible results when resuming from checkpoints
            with numpy_seed(self.seed + epoch):
                batches = list(self.frozen_batches)  # copy
                np.random.shuffle(batches)
        else:
            batches = self.frozen_batches
        return CountingIterator(torch.utils.data.DataLoader(
            self.dataset,
            collate_fn=self.dataset.collater,
            batch_sampler=ShardedIterator(batches, self.num_shards, self.shard_id, fill_value=[]),
        ))

    def _batch_generator(self):
        batch = []

        # 判断这个batch是否是满的
        def is_batch_full(num_tokens):
            if len(batch) == 0:
                return False
            if len(batch) == self.max_sentences:
                return True
            if num_tokens > self.max_tokens:
                return True
            return False

        sample_len = 0
        sample_lens = []
        ignored = []
        # print(self.dataset.ordered_indices())
        for idx in self.dataset.ordered_indices():
            # if not self.dataset.valid_size(idx, self.max_positions):
            #     if self.ignore_invalid_inputs:
            #         ignored.append(idx)
            #         continue
            #     raise Exception((
            #         'Size of sample #{} is invalid, max_positions={}, skip this '
            #         'example with --skip-invalid-size-inputs-valid-test'
            #     ).format(idx, self.max_positions))

            sample_lens.append(self.dataset.num_tokens(idx))
            sample_len = max(sample_len, sample_lens[-1])
            num_tokens = (len(batch) + 1) * sample_len
            if is_batch_full(num_tokens):
                mod_len = max(
                    self.bsz_mult * (len(batch) // self.bsz_mult),
                    len(batch) % self.bsz_mult,
                )
                yield batch[:mod_len]
                batch = batch[mod_len:]
                sample_lens = sample_lens[mod_len:]
                sample_len = max(sample_lens) if len(sample_lens) > 0 else 0

            batch.append(idx)

        if len(batch) > 0:
            yield batch

        if len(ignored) > 0:
            print((
                '| WARNING: {} samples have invalid sizes and will be skipped, '
                'max_positions={}, first few sample ids={}'
            ).format(len(ignored), self.max_positions, ignored[:10]))



@contextlib.contextmanager
def numpy_seed(seed):
    """Context manager which seeds the NumPy PRNG with the specified seed and
    restores the state afterward"""
    if seed is None:
        yield
        return
    state = np.random.get_state()
    np.random.seed(seed)
    try:
        yield
    finally:
        np.random.set_state(state)