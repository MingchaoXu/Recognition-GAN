python -u train_reg_v3.py --device_id 1 \
 --crop_size 32 \
 --config cfg/config-v3-verify.yaml \
 --fine_G ../epochs/v3-verify/netG_epoch_4_2_iter_0.pth \
  --fine_D ../finetune_model/crnn_recognition.pth \
 --test \
--generate \
  #--fine_D ../epochs/v2-1,0.1/netD_epoch_4_1_iter_10000.pth \
#  --fine_D ../finetune-model/checkpoint130.pt
