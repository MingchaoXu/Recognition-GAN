from torch import nn


class SELayer(nn.Module):
    def __init__(self, channel, reduction=16):
        super(SELayer, self).__init__()
        # self.avg_pool = nn.AdaptiveAvgPool2d(1)
        self.deconv1 = nn.ConvTranspose2d(512, 64, kernel_size=(2, 1), stride=(2, 1))
        self.deconv2 = nn.ConvTranspose2d(64, 64, kernel_size=(2, 1), stride=(2, 1))
        self.deconv3 = nn.ConvTranspose2d(64, 64, kernel_size=(2, 1), stride=(2, 1))
        # self.pixel_shuffle = nn.PixelShuffle(up_scale)
        # self.fc = nn.Sequential(
        #     nn.Linear(channel, channel // reduction, bias=False),
        #     nn.ReLU(inplace=True),
        #     nn.Linear(channel // reduction, channel, bias=False),
        #     nn.Sigmoid()
        # )

    def forward(self, x, q):
        b, c, _, _ = x.size()
        # print('q', q.size())
        q = self.deconv1(q)
        q = self.deconv2(q)
        q = self.deconv3(q)
        # print('q', q.size())

        return x.mul(q)