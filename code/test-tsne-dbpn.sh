python test-tsne-dbpn.py --device_id 1 \
 --crop_size 32 \
 --config cfg/config-dbpn1.yaml \
 --fine_G ../epochs/37class-1-0.1weight-dbpn1/netG_epoch_4_4_iter_80000.pth \
  --fine_D ../epochs/37class-1-0.1weight-dbpn1/netD_epoch_4_4_iter_80000.pth \
 --test \
#  --fine_G ../epochs/v2-1,0.1/netG_epoch_4_1_iter_180000.pth \
  #-fine_D ../epochs/v2-1,0.1/netD_epoch_4_1_iter_180000.pth \
# --generate \
#  --fine_D ../finetune-model/checkpoint130.pt
