# train_id=$1

# if [ $train_id = "trainv1" ]; then
#     list_param="--tsne_input=feature_beard_trainv1.npy --target=target_beard_trainv1.npy"
# elif [ $train_id = "trainv2" ]; then
#     list_param="--tsne_input=feature_beard_trainv2.npy --target=target_beard_trainv2.npy"
# elif [ $train_id = "trainv3" ]; then
#     list_param="--tsne_input=feature_beard_trainv3.npy --target=target_beard_trainv3.npy"
# fi

# echo $list_param
JOB_NAME=$2
GLOG_vmodule=MemcachedClient=-1 \
    srun --mpi=pmi2 -p $1 \
    --job-name=$JOB_NAME \
 python -u tsne.py \
 --root /mnt/lustre/xumingchao1/experiment/SRGAN-reg-eng/code \
 --sr_matri ../tsne/iam-v2-2-1000len/netD_epoch_4_49.pth_sr.npy \
 --hr_matri ../tsne/iam-v2-2-1000len/netD_epoch_4_49.pth_hr.npy \
 --hr_restore_matri ../tsne/iam-v2-2-1000len/netD_epoch_4_49.pth_hr_restore.npy \
 --out result \
#  --sr_matri ../tsne/v2-1,0.1/netD_epoch_4_1_iter_180000.pth_sr.npy \
#  --hr_matri ../tsne/v2-1,0.1/netD_epoch_4_1_iter_180000.pth_hr.npy \
#  --hr_restore_matri ../tsne/v2-1,0.1/netD_epoch_4_1_iter_180000.pth_hr_restore.npy \
 #  --sr_matri ../tsne/iam-crnn-v1-2-1000len-reg_fake_w1/netD_epoch_4_15.pth_sr.npy \
#  --hr_matri ../tsne/iam-crnn-v1-2-1000len-reg_fake_w1/netD_epoch_4_15.pth_hr.npy \
#  --hr_restore_matri ../tsne/iam-crnn-v1-2-1000len-reg_fake_w1/netD_epoch_4_15.pth_hr_restore.npy \
#  --sr_matri ../tsne/iam-crnn-v2-2/netD_epoch_4_199.pth_sr.npy \
#  --hr_matri ../tsne/iam-crnn-v2-2/netD_epoch_4_199.pth_hr.npy \
#  --hr_restore_matri ../tsne/iam-crnn-v2-2/netD_epoch_4_199.pth_hr_restore.npy \
# --sr_matri ../tsne/iam-crnn-v1-2/netD_epoch_4_197.pth_sr.npy \
#  --hr_matri ../tsne/iam-crnn-v1-2/netD_epoch_4_197.pth_hr.npy \
#  --hr_restore_matri ../tsne/iam-crnn-v1-2/netD_epoch_4_197.pth_hr_restore.npy \
# --sr_matri ../tsne/iam-crnn-v1-2/netD_epoch_4_197.pth_sr-black.npy \
#  --hr_matri ../tsne/iam-crnn-v1-2/netD_epoch_4_197.pth_hr-black.npy \
#  --hr_restore_matri ../tsne/iam-crnn-v1-2/netD_epoch_4_197.pth_hr_restore-black.npy \
#  --tsne_input feature_person_trainv1.npy \
#  --target target_person_trainv1.npy \
