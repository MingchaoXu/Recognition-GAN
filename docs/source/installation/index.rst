Installation
===============================

.. toctree::
   :maxdepth: 2

.. note::
    请先按照教程装好环境

Basic Installation
-----------------------

精简安装： 急速部署，针对只需要进行模型训练的情况.

.. code-block:: bash

    git clone https://gitlab.com/MingchaoXu/Recognition-GAN.git
    cd Recognition-GAN

    conda create -n recgan python=3.6 
    conda activate recgan

    conda install pytorch=0.4.1 torchvision=0.2.2 -c pytorch

    python -m pip install -r requirements.txt --user

