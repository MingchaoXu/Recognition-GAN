.. Recognition-GAN project documentation master file, created by
   sphinx-quickstart on Fri Mar 13 21:36:41 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Recognition-GAN's documentation!
===============================


Recognition GAN是一套识别模糊文本的pytorch 框架.

.. code-block:: 

    ____                            _ _   _                    ____    _    _   _
   |  _ \ ___  ___ ___   __ _ _ __ (_) |_(_) ___  _ __        / ___|  / \  | \ | |
   | |_) / _ \/ __/ _ \ / _` | '_ \| | __| |/ _ \| '_ \ _____| |  _  / _ \ |  \| |
   |  _ <  __/ (_| (_) | (_| | | | | | |_| | (_) | | | |_____| |_| |/ ___ \| |\  |
   |_| \_\___|\___\___/ \__, |_| |_|_|\__|_|\___/|_| |_|      \____/_/   \_\_| \_|
                        |___/
                        



支持模糊文本数据的识别，以及tsne特征可视化等功能.

- 项目主页:  https://mingchaoxu.gitlab.io/Recognition-GAN
- 代码仓库:  https://gitlab.com/MingchaoXu/Recognition-GAN
- 部署环境:  pytorch 0.4.1

.. toctree::
   :maxdepth: 2
   :caption: Table of Contents

   installation/index
   tutorial/index
   faq/index


   
