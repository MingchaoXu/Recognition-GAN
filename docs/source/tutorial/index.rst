Tutorial
===============================

.. toctree::
   :maxdepth: 2



.. code-block:: bash

    .
    ├── code              # 存放训练测试代码和实验配置yaml
    ├── docs              # 存放说明文档
    ├── finetune_model    # 存放finetune model
    ├── paralell-tsne     # 存放tsne可视化脚本
    ├── requirements.txt  # 项目依赖包
    ├── scripts           # 依赖脚本
    └── tensorboard       # tensorboard 可视化脚本

Train
------------------------------------------

1. 根据需求修改或创建yaml文件，如 ``config-iam-crnn-v1.yaml``，配置文件的具体说明见 `配置文件说明`_ 

2. 开始训练：

.. code-block:: bash

    Usage: train <JOB_NAME> <DEVICE_ID> <CONFIG>
    
    # example
    bash train.sh 8 config-iam-crnn-v1.yaml train_job


Test
------------------------------------------


1. 根据需求修改或创建测试yaml文件，如 ``config-iam-crnn-v1.yaml``，主要参考 `配置文件说明`_  中的fine_D和fine_G字段

.. code-block:: yaml

    fine_D: ../epochs/scene-v0-5/netD_epoch_4_1_iter_25000.pth
    fine_G: ../epochs/scene-v0-5/netG_epoch_4_1_iter_25000.pth

2. 开始测试：

.. code-block:: bash

    Usage: test <JOB_NAME> <DEVICE_ID> <CONFIG>
    
    # example
    bash test.sh 8 config-iam-crnn-v1.yaml test



配置文件说明
----------------------------------

:配置文件一览:
.. code-block:: yaml

    common:
        workers: 1
        trainbatch: 12
        testbatch: 12
        model: model_iam_crnn_v2

        num_epochs: 200
        warmup_steps: 0
        warmup_lr: 0.4
        base_lr: 0.001
        D_lr: [0.1]
        lr: [0.1]
        lr_shrink: 0.1
        reg_w: 1
        reg_fake_w: 0.1
        verify_w: 0.1
        lr_steps: [1000, 2000, 3000]
        lr_mults: [0.1, 0.1, 0.1]
        optimizerD: adam
        adam_betas: '(0.975, 0.999)'
        adam_eps: 1e-8
        lr_scheduler: reduce_lr_on_plateau
        recognition_result: ../image_val/v2-2.txt

        clip_norm: 400
        momentum: 0.99 # 0.9
        weight_decay: 0 # 0.0001
        sronly: False
        regonly: False
        verifyonly: True

        val_freq: 5000
        print_freq: 10
        tsne_path: ../tsne/iam-v2-2-1000len
        save_path: ../training_results/iam-crnn-v2-2-1000len
        model_path: ../epochs/iam-crnn-v2-2-1000len
        generate_img: /data2/mcxu/FormularDatabase/iam/generate_img-crnn-v2-2-1000len
        veri_model_path: ../finetune_model/crnn_recognition.pth
        fine_D: ../epochs/iam-crnn-v2-2-1000len/netD_epoch_4_49.pth
        fine_G: ../epochs/iam-crnn-v3/netG_epoch_4_19.pth

        val_lmdb_path: /home/lustre/mcxu/experiment/iamLMDB/test
        train_lmdb_path: /home/lustre/mcxu/experiment/iamLMDB/train
        dict_name: /home/lustre/mcxu/experiment/iamLMDB/iam.dict

:模型相关设置:
.. code-block:: yaml

    workers: 1                       # pytorch dataloader并行线程数，可以显著增加训练速度
    trainbatch: 12                   # 训练集batch size
    testbatch: 12                    # 测试集batch size
    model: model_iam_crnn_v2         # 模型文件选择
    sronly: False                    # False,False,False 代表recognition gan
    regonly: False
    verifyonly: True

:训练超参设置:
.. code-block:: yaml

    num_epochs: 200                  # 最大训练epoch数
    base_lr: 0.001                   # 基础lr
    D_lr: [0.1]                      # discriminator lr
    reg_w: 1                         # 真实样本识别loss权重
    reg_fake_w: 0.1                  # 生成样本识别loss权重
    lr_steps: [1000, 2000, 3000]     # lr 改变代数
    lr_mults: [0.1, 0.1, 0.1]        # lr 改变比例
    optimizerD: adam                 # discriminator optimizer
    adam_betas: '(0.975, 0.999)'     # Adam betas 参数
    adam_eps: 1e-8                   # Adam eps参数
    clip_norm: 400                   # clip_norm值
    momentum: 0.99                   # momentum值
    weight_decay: 0                  # weight decay值

:数据集设置:
本项目支持两种任务的数据集：scene text recognition task 和 handwritten recognition task

lmdb 数据集设置方式

.. code-block:: yaml

    val_lmdb_path: /home/lustre/mcxu/experiment/iamLMDB/test
    train_lmdb_path: /home/lustre/mcxu/experiment/iamLMDB/train

正常图片数据集设置方式

.. code-block:: yaml

    source_file: /data2/mcxu/FormularDatabase/iam/train_mc.list
    train_root: /data2/mcxu/FormularDatabase/iam

Data Augmentation
------------------------------------------

目前训练支持多种数据增强, 包括:


- resize
- normalize
- crop
- gaussian_blur



调参经验Q&A
---------------------

这一节主要列出关于模型训练的一些小经验

1. 关于预训练模型

本项目实验中，加入预训练模型会加速收敛，不过实际上训练不加预训练模型也可以收敛到预期的效果。

2. 关于学习率调节

实验过程中，scene text recognition的task采用的是通用的sgd，handwritten recognition task 采用的是adam，如果实验发现gan收敛的较慢，可以适当延长训练epoch看看效果。
我尝试的学习率一般是0.001。

3. 关于batchsize

一般情况下batchsize越大对精度有正面效果，不过训练时batchsize 取12左右即可有一个较高的精度。

