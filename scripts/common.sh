#!/bin/bash

## blue to echo
function info_blue(){
    echo -e "\033[35m[ $1 ]\033[0m"
    return
}

## green to echo
function info_green(){
    echo -e "\033[32m[ $1 ]\033[0m"
    return
}

## Error
function info_red(){
    echo -e "\033[31m\033[01m[ $1 ]\033[0m"
    return
}

## warning
function info_yellow(){
    echo -e "\033[33m\033[01m[ $1 ]\033[0m"
    return
}

## is fake mode?
function fake_mode() {
    fake_mode_var=$(echo $PBA_FAKE_MODE | awk '{print tolower($0)}')
    if ([ "$fake_mode_var" == "true" ] || [ "$fake_mode_var" == "on" ]); then
        return 0
    else
        return 1
    fi
}

## check python env
function check_python_env() {
    python_env=$1
    v=$(which python | sed -e "s/.*\/\($python_env\)\/.*/\1/g")
    if [ $v == $python_env ]; then
        return 0
    else
        return 1
    fi
}
